#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'rafael'

from django import template
from django.contrib.auth.models import Group
import locale
locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
register = template.Library()


@register.filter(name='distanceformat')
def distanceformat(value, places=1):
    format_string = "%%.%df km" % places
    return format_string % value.km