#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from advertiser.models import Account
from django.utils.text import slugify

TRANSACAO = (
    ('V', 'Venda'),
    ('L', 'Locação')
)

LOGTYPE = (
    ('A', 'Acesso'),
    ('T', 'Telefone'),
    ('C', 'Contato')
)


class Categoria(models.Model):
    nome = models.CharField(max_length=255, null=False, blank=False)
    slug = models.SlugField(unique=True, blank=True, null=True)
    pai = models.ForeignKey('self')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nome)
        super(Categoria, self).save(*args, **kwargs)

    def __str__(self):
        return self.nome


class Propriedade(models.Model):
    account = models.ForeignKey(Account)
    referer = models.CharField(max_length=70, null=False, blank=False)
    transacao = models.CharField(max_length=1, null=False, blank=False, choices=TRANSACAO)
    categoria = models.ForeignKey(Categoria, null=True, blank=True)
    valor_venda = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)
    valor_aluguel = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)
    area_privada = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)
    area_total = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)
    quartos = models.IntegerField(null=True, blank=True)
    suites = models.IntegerField(null=True, blank=True)
    banheiros = models.IntegerField(null=True, blank=True)
    vagas = models.IntegerField(null=True, blank=True)
    descricao = models.TextField(null=True, blank=True)
    titulo = models.TextField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True, default=None)
    longitude = models.FloatField(null=True, blank=True, default=None)
    deletado = models.DateField(null=True, blank=True, default=None)

    endereco = models.CharField(max_length=500, blank=True, null=True)
    complemento = models.CharField(max_length=500, blank=True, null=True)
    numero = models.CharField(max_length=500, blank=True, null=True)


class FotoQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(deletado__isnull=True)


class FotoManager(models.Manager):
    use_for_related_fields = True

    def get_queryset(self):
        return FotoQuerySet(self.model)

    def get_active(self, *args, **kwargs):
        return self.get_queryset().active(*args, **kwargs)


class Fotos(models.Model):
    url = models.TextField(null=False, blank=False)
    url_aws = models.TextField(blank=True, null=True, default=None)
    posicao = models.IntegerField(blank=True, null=True, default=None)
    deletado = models.DateField(auto_now_add=False, auto_now=False, null=True, blank=True)
    propriedade = models.ForeignKey(Propriedade)
    objects = FotoManager()

    def __str__(self):
        return self.url_aws if self.url_aws else self.url


class Log(models.Model):
    propriedade = models.ForeignKey(Propriedade)
    type = models.CharField(max_length=1, choices=LOGTYPE)
    cadastro = models.DateField(auto_now_add=True)