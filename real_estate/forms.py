#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from real_estate.models import Propriedade


class PropriedadeForm(forms.ModelForm):
    titulo = forms.CharField()

    class Meta:
        model = Propriedade
        fields = ['referer', 'transacao', 'categoria', 'valor_venda', 'valor_aluguel', 'area_privada', 'area_total',
                  'quartos', 'suites', 'banheiros', 'vagas', 'descricao', 'titulo', 'latitude', 'longitude',
                  'endereco', 'complemento', 'numero']