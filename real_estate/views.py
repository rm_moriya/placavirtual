__author__ = 'rafael'
from django.shortcuts import render
from real_estate.models import Propriedade, Log
from advertiser.forms import ContatoForm


def listing(request, id, name):
    return render(request, 'listing.html', {})


def listing_map(request, id, name):
    return render(request, 'listing_map.html', {})


def propriedade(request, id):
    propriedade = Propriedade.objects.get(pk=id)
    Log(propriedade=propriedade, type='A').save()
    form = ContatoForm(initial={'mensagem': 'Tenho interesse neste imovel (Ref.:' + propriedade.referer + ') que encontrei no Placavirtual.com.br. Aguardo o contato. Obrigado'})

    return render(request, 'propriedade.html', {
        'real_estate': propriedade,
        'form': form
    })