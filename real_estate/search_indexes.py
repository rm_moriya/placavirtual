#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'rafael'
from haystack import indexes
from .models import Propriedade


class PropertyIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo', null=True)
    account = indexes.IntegerField(model_attr='account__pk', null=True)
    transacao = indexes.CharField(model_attr='transacao', null=True)

    valor_venda = indexes.FloatField(model_attr="valor_venda", null=True)
    valor_aluguel = indexes.FloatField(model_attr="valor_aluguel", null=True)

    longitude = indexes.FloatField(model_attr="longitude", null=True)
    latitude = indexes.FloatField(model_attr="latitude", null=True)
    location = indexes.LocationField(null=True)

    deletado = indexes.DateField(model_attr='deletado', null=True)

    area_privada = indexes.FloatField(model_attr="area_privada", null=True)
    area_total = indexes.FloatField(model_attr="area_total", null=True)
    quartos = indexes.IntegerField(model_attr='quartos', null=True)
    banheiros = indexes.IntegerField(model_attr='banheiros', null=True)
    vagas = indexes.IntegerField(model_attr='vagas', null=True)
    suites = indexes.IntegerField(model_attr='suites', null=True)

    account_vencimento = indexes.DateField(model_attr='account__vencimento', null=True)

    photo = indexes.MultiValueField(indexed=True, stored=True)

    def get_model(self):
        return Propriedade

    def prepare_location(self, obj):
        if obj.latitude and obj.longitude:
            return "%s,%s" % (obj.longitude, obj.latitude)

    def prepare_photo(self, obj):
       return [photo.url_aws if photo.url_aws else photo.url for photo in obj.fotos_set.filter(deletado__isnull=True)]