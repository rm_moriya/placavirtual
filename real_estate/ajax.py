__author__ = 'rafael'
import json
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from haystack.query import SearchQuerySet
from haystack.utils.geo import D, Point
from real_estate.models import Propriedade


@csrf_exempt
def lista_mapa(request):
    suggestions = {}
    properties = SearchQuerySet().models(Propriedade)
    properties = properties.filter(account=request.POST.get('account'))
    radius = D(km=100)
    center = Point(float(request.POST.get('latitude')), float(request.POST.get('longitude')))

    properties = properties.dwithin('location', center, radius)
    # properties = properties.filter(account_vencimento__gte=datetime.now(), _missing_='date_deleted')
    properties = properties.distance('location', center).order_by('distance')

    show_maps = []
    for property in properties:
        html = get_template('unique.html')
        d = Context({
            'real_estate': property
        })

        show_maps.append({
            'pk': str(property.pk),
            'latitude': str(property.latitude),
            'longitude': str(property.longitude),
            'transacao': str(property.transacao),
            'listing': html.render(d)
        })

    suggestions['properties'] = show_maps
    return HttpResponse(json.dumps(suggestions), content_type='application/json')
