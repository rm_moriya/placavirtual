"""Defaults urls for the Zinnia project"""
from django.conf.urls import url
from django.conf.urls import include

urlpatterns = [
    url(r'^propriedade/(?P<id>[\d]+)$', 'real_estate.views.propriedade', name='propriedade'),
    url(r'^ajax/', include('real_estate.urls.ajax')),
]