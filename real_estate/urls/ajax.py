"""Urls for the Zinnia comments"""
from django.conf.urls import url

urlpatterns = [
    url(r'^imoveis-maps/$', 'real_estate.ajax.lista_mapa', name='ajax_real_estate_listing')
]