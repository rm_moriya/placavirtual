#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from real_estate.models import Propriedade


class PropriedadeAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(PropriedadeAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(account=request.user)

admin.site.register(Propriedade, PropriedadeAdmin)

