#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.utils.safestring import mark_safe
from advertiser.models import Account, Contato, Xml

EMPTY_CHOICES = (
    (u'', u'---------'),
)


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['avatar', 'type', 'name', 'email', 'email_financeiro', 'email_marketing', 'phone', 'cellphone',
                  'first_name', 'document']

    def clean_email(self):
        email = self.cleaned_data.get('email')
        existing = Account.objects.filter(email__iexact=email).exclude(pk=self.instance.pk).exists()

        if existing:
            raise forms.ValidationError(mark_safe(u"'<strong>%s</strong>' já está cadastrado!" % (email)))

        return email


class ContatoForm(forms.ModelForm):
    class Meta:
        model = Contato
        fields = ['nome', 'email', 'telefone', 'mensagem', 'propriedade', 'account']


class XmlForm(forms.ModelForm):
    class Meta:
        model = Xml
        fields = ['url']
