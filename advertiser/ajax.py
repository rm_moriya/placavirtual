__author__ = 'rafael'
import json
from django.http import HttpResponse
from advertiser.forms import ContatoForm


def contato(request):
    form = ContatoForm(request.POST or None)
    suggestions = {'status': False}
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            suggestions['status'] = True

    return HttpResponse(json.dumps(suggestions), content_type='application/json')