__author__ = 'rafael'
from django.shortcuts import render
from advertiser.models import Account


def listing(request, id, name):
    account = Account.objects.get(pk=id)
    return render(request, 'listing.html', {
        'account': account
    })


def listing_map(request, id, name):
    account = Account.objects.get(pk=id)
    return render(request, 'listing_map.html', {
        'account': account
    })