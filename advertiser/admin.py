#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from advertiser.models import Account, Xml


class AccountAdmin(UserAdmin):
    UserAdmin.list_display = ('first_name', 'document', 'email', 'name', 'phone', 'plan', 'is_active')
    UserAdmin.fieldsets += (('Plano', {'fields': ('plan',)}),)

    def get_queryset(self, request):
        qs = super(AccountAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(groups__name="Imobiliaria")

admin.site.register(Account, AccountAdmin)


class XmlAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(XmlAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(groups__name="Imobiliaria")

admin.site.register(Xml, XmlAdmin)
