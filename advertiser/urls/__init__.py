"""Defaults urls for the Zinnia project"""
from django.conf.urls import url
from django.conf.urls import include

urlpatterns = [
    url(r'^', include('advertiser.urls.site')),
    url(r'^ajax/', include('advertiser.urls.ajax')),
]