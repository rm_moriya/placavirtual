"""Urls for the Zinnia comments"""
from django.conf.urls import url

urlpatterns = [
    url(r'^contato/$', 'advertiser.ajax.contato', name='contato')
]