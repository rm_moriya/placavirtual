"""Urls for the Zinnia comments"""
from django.conf.urls import url

urlpatterns = [
    url(r'^(?P<id>[\d]+)-(?P<name>[\w-]+)/$', 'advertiser.views.listing', name='advertiser'),
    url(r'^map/(?P<id>[\d]+)-(?P<name>[\w-]+)/$', 'advertiser.views.listing_map', name='advertiser_map'),
]