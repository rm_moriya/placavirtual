#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from datetime import datetime, date
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils.text import slugify
from iugu.customer import Customer as IuguCustomer
from iugu.subscription import Subscription as IuguSubscription


TYPES = (
    ('', 'Perfil'),
    ('CON', 'Construtora'),
    ('IMO', 'Imobiliária'),
    ('COR', 'Corretor'),
    ('PRO', 'Proprietário')
)


class ProfileManager(BaseUserManager):
    def create_user(self, username, email, password='padrao123'):
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(username=username, email=self.normalize_email(email))
        user.is_active = True
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username=username, email=email, password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


def avatar_directory_path(instance, filename):
    path = "advertiser/avatar"
    ext = filename.split('.')[-1]
    format = slugify(instance.name + ' ' + str(instance.id)) + '.' + ext
    return os.path.join(path, format)


class Account(AbstractBaseUser, PermissionsMixin):
    objects = ProfileManager()
    # basic fields
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', message='Somente alfanuméricos é permitido.')
    username = models.CharField(unique=True, max_length=125, validators=[alphanumeric])
    email = models.EmailField(verbose_name='email address', unique=True, max_length=255)
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    date_joined = models.DateTimeField(default=datetime.now)
    is_active = models.BooleanField(default=True, null=False)
    is_staff = models.BooleanField(default=False, null=False)
    avatar = models.ImageField(upload_to=avatar_directory_path, blank=True, null=True)

    date_register = models.DateTimeField(auto_now_add=True)
    type = models.CharField(blank=False, null=False, choices=TYPES, max_length=3)
    name = models.CharField(verbose_name=u'Responsável', max_length=255)
    email = models.EmailField(verbose_name='Email', unique=True, max_length=255)
    email_financeiro = models.EmailField(max_length=255, default=None, blank=True, null=True)
    email_marketing = models.EmailField(max_length=255, default=None, blank=True, null=True)
    phone = models.CharField(verbose_name='Telefone', max_length=15, null=True, blank=True)
    cellphone = models.CharField(max_length=15, null=True, blank=True)
    vencimento = models.DateField(null=True, blank=True, default=None)
    referer = models.CharField(max_length=255, null=True, blank=True, default=None)

    iugu_id = models.CharField(max_length=32, null=True, blank=True, default=None)
    document = models.CharField(verbose_name='CPF/CNPJ', max_length=18, null=True, blank=True, default=None)

    plan = models.ForeignKey('base.Plano', null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.email

    def get_short_name(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Account, self).save(*args, **kwargs)

        if self.first_name:
            self.username = str(slugify(self.first_name + str(self.id))).replace('-','')

        if self.iugu_id in [None, '']:
            client = IuguCustomer()
            customer = client.create({'email': self.email, 'name': self.first_name, 'cpf_cnpj': self.document})
            self.iugu_id = customer.get('id')

        if self.plan:
            subs = IuguSubscription()
            assinatura = subs.create({'customer_id': self.iugu_id, 'plan_identifier': self.plan.iugu_id })
            self.assinatura_set.get_or_create(iugu_id=assinatura.get('id'), plano=self.plan)

        super(Account, self).save(*args, **kwargs)

    @property
    def is_past_due(self):
        if self.vencimento not in ['', None] and date.today() > self.vencimento:
            return True
        return False

    @property
    def expirate(self):
        if self.vencimento:
            return (date.today()-self.vencimento).days
        return 'Esperando pagamento'


class Xml(models.Model):
    url = models.CharField(max_length=500, null=False, blank=False)
    account = models.ForeignKey(Account)
    cadastrado = models.DateField(auto_now_add=True)
    lido = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return unicode(self.url)


class Contato(models.Model):
    account = models.ForeignKey(Account)
    propriedade = models.ForeignKey('real_estate.Propriedade', null=True, blank=True)
    nome = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=255, blank=False, null=False)
    telefone = models.CharField(max_length=20, blank=True, null=True)
    mensagem = models.TextField(blank=False, null=False)
    cadastrado = models.DateField(auto_now_add=True)
