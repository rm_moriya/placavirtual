#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'rafael'

import locale
from datetime import datetime
from dateutil.relativedelta import relativedelta
from django import template
from django.db.models import Count
from real_estate.models import Log
register = template.Library()
locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
register = template.Library()


@register.filter(name='distanceformat')
def distanceformat(value, places=1):
    format_string = "%%.%df km" % places
    return format_string % value.km


@register.inclusion_tag('advertiser_admin/grafico.html')
def grafico(propriedade, tipo):
    logs = Log.objects.filter(propriedade=propriedade, type=tipo, cadastro__gte=datetime.now()-relativedelta(months=1),
                              cadastro__lte=datetime.now()).extra({'cadastro': "date(cadastro)"})\
        .values('cadastro')\
        .annotate(total=Count('id'))
    return {
        'logs': logs,
        'tipo': tipo
    }