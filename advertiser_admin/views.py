#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from advertiser.forms import AccountForm, XmlForm
from django.contrib import messages
from advertiser.models import Contato, Xml, Account
from real_estate.models import Propriedade
from real_estate.forms import PropriedadeForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.management import call_command

from iugu.subscription import Subscription as IuguSubscription


def auth(request):

    form = AccountForm()
    context = {'form': form, 'error': False}

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            if request.GET.get('next'):
                return HttpResponseRedirect(request.GET.get('next'))
            else:
                return HttpResponseRedirect(reverse('dashboard'))
        else:
            messages.error(request, u'Usuário ou Senha inválido')

    return render(request, "advertiser_admin/auth.html", context)


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('auth_page'))


@login_required
def account(request):
    form = AccountForm(request.POST or None, request.FILES or None, instance=Account.objects.get(pk=request.user.pk))

    if request.method == 'POST':
        if form.is_valid():
            form.save()

    return render(request, 'advertiser_admin/account.html', {
        'form': form
    })


@login_required
def dashboard(request):
    return render(request, 'advertiser_admin/dashboard.html', {})


@login_required
def interessados(request):
    contatos = Contato.objects.filter(account=request.user.pk)
    return render(request, 'advertiser_admin/interessados.html', {
        'contatos': contatos
    })


@login_required
def interessado(request, id):
    return render(request, 'advertiser_admin/interessado.html', {
        'contato': Contato.objects.get(pk=id)
    })


@login_required
def deletar_interessado(request, id):
    Contato.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('painel_interessado'))


@login_required
def xml(request):
    xmls = Xml.objects.filter(account=request.user.pk)
    return render(request, 'advertiser_admin/xml.html', {
        'xmls': xmls,
        'form': XmlForm()
    })


@login_required
def cadastrar_xml(request):
    form = XmlForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            obj = form.save(commit=False)
            obj.account = request.user
            obj.save()
            call_command('importacao', '--id=' + str(obj.account.pk))
            return HttpResponseRedirect(reverse('painel_xml'))

@login_required
def deletar_xml(request, id):
    Xml.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('painel_xml'))


@login_required
def imoveis(request):
    imoveis = Propriedade.objects.filter(account=request.user.pk)
    total = imoveis.count()

    paginator = Paginator(imoveis, 50)
    page = request.GET.get('page')
    try:
        imoveis = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        imoveis = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        imoveis = paginator.page(paginator.num_pages)

    return render(request, 'advertiser_admin/imoveis.html', {
        'imoveis': imoveis,
        'total': total
    })


@login_required
def deletar_imovel(request, id):
    Propriedade.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('painel_imoveis'))


@login_required
def editar_imovel(request, id):
    imovel = Propriedade.objects.get(pk=id)
    form = PropriedadeForm(request.POST or None, instance=imovel)

    return render(request, 'advertiser_admin/dados_imovel.html', {
        'imovel': imoveis,
        'form': form
    })


@login_required
def estatistica_imovel(request, id):
    imovel = Propriedade.objects.get(pk=id)
    return render(request, 'advertiser_admin/imovel_estatistica.html', {
        'imovel': imovel
    })


def pagamento(request):

    # client = IuguCustomer()
    # user = request.user
    # if user.iugu_id:
    #     customer = client.get(customer_id=user.iugu_id)
    # else:
    #     customer = client.create(email=user.email, name=user.first_name)
    #     user.iugu_id = customer.id
    #     user.save()

    subscription = IuguSubscription.get('8C060A1A77D14EAABB2380132AF1A032')
    subscription.remove()
    # plan_x = IuguPlan().create(name='Ilimitado 199', identifier='plano_ilimitado_199', interval='1', interval_type='months',
    #                            currency="BRL", value_cents=19900)
    # client = IuguSubscription()
    # client.create(customer_id=customer.id, plan_identifier='BDB69000777F41A0835114837CAF3191')
    # client.create(customer_id=customer.id, plan_identifier=plan_x.identifier)

@login_required
def plano(request):
    return render(request, 'advertiser_admin/plano.html', {
        'advertiser': Account.objects.get(pk=request.user.pk)
    })