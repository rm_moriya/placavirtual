__author__ = 'rafael'
from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'advertiser_admin.views.auth', name="auth_page"),
    url(r'^logout/$', 'advertiser_admin.views.logout_view', name="logout"),

    url(r'^pagamento/$', 'advertiser_admin.views.pagamento', name="pagamento"),

    url(r'^imoveis/$', 'advertiser_admin.views.imoveis', name="painel_imoveis"),
    url(r'^imoveis/(?P<id>\d+)/$', 'advertiser_admin.views.editar_imovel', name="painel_editar_imovel"),
    url(r'^imoveis/estatistica/(?P<id>\d+)/$', 'advertiser_admin.views.estatistica_imovel', name="painel_estatistica_imovel"),
    url(r'^imoveis/deletar/(?P<id>\d+)/$', 'advertiser_admin.views.deletar_imovel', name="painel_deletar_imovel"),

    url(r'^minha-conta/$', 'advertiser_admin.views.account', name="painel_account"),
    url(r'^plano/$', 'advertiser_admin.views.plano', name="painel_plano"),

    url(r'^dashboard/$', 'advertiser_admin.views.dashboard', name="dashboard"),

    url(r'^interessado/$', 'advertiser_admin.views.interessados', name="painel_interessado"),
    url(r'^interessado/(?P<id>\d+)/$', 'advertiser_admin.views.interessado', name="painel_view_interessado"),
    url(r'^interessado/deletar/(?P<id>\d+)/$', 'advertiser_admin.views.deletar_interessado', name="painel_deletar_interessado"),

    url(r'^xml/$', 'advertiser_admin.views.xml', name="painel_xml"),
    url(r'^xml/cadastrar/$', 'advertiser_admin.views.cadastrar_xml', name="painel_cadastrar_xml"),
    url(r'^xml/deletar/(?P<id>\d+)/$', 'advertiser_admin.views.deletar_xml', name="painel_deletar_xml"),
]
