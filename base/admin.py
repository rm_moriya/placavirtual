#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from base.models import Plano


class PlanoAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(PlanoAdmin, self).get_queryset(request)
        return qs

admin.site.register(Plano, PlanoAdmin)

