#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from advertiser.forms import AccountForm
from django.http import HttpResponseRedirect
from django.contrib import messages
from iugu.subscription import Subscription


def home(request):
    Subscription().remove('1A18553258D246448DB525767E16799A')
    return render(request, 'home.html', {})


def anuncie(request):
    form = AccountForm(request.POST or None)
    context = {'form': form, 'status': False}

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "Cadastro realizado com sucesso! Em breve entraremos em contato!")
            return HttpResponseRedirect(request.path)
        else:
            messages.error(request, "Preencha todos os campos!")

    return render(request, 'anuncie.html', context)