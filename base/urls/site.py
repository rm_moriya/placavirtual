"""Urls for the Zinnia comments"""
from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'base.views.home', name='home'),
    url(r'^anuncie/$', 'base.views.anuncie', name='anuncie'),
]