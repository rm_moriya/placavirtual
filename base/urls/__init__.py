"""Defaults urls for the Zinnia project"""
from django.conf.urls import url
from django.conf.urls import include

urlpatterns = [
    url(r'^', include('base.urls.site')),
]