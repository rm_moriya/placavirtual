#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.utils.text import slugify
from iugu.plan import Plan as IuguPlan


class Plano(models.Model):
    nome = models.CharField(max_length=255, null=False, blank=False)
    iugu_id = models.CharField(max_length=32, null=True, blank=True)
    preco = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)

    def __unicode__(self):
        return unicode(self.nome)

    def save(self, *args, **kwargs):
        super(Plano, self).save(*args, **kwargs)
        plan = IuguPlan()
        if self.iugu_id in [None, '']:
            plan = plan.create({'name': self.nome, 'identifier': slugify(self.nome), 'interval': 1,
                                'interval_type': 'months', 'currency': "BRL",
                                'value_cents': str(self.preco).replace('.', '')})
            self.iugu_id = plan.get('id')
        else:
            plan.change(self.iugu_id, {'name': self.nome, 'identifier': slugify(self.nome), 'interval': 1,
                                       'interval_type': 'months', 'currency': "BRL",
                                       'value_cents': str(self.preco).replace('.', '')})

        super(Plano, self).save(*args, **kwargs)


class Assinatura(models.Model):
    iugu_id = models.CharField(max_length=32, null=True, blank=True, help_text='Id da assinatura')
    data_criacao = models.DateField(auto_now_add=True)
    ativa = models.BooleanField(default=False)
    account = models.ForeignKey('advertiser.Account', default=None)
    plano = models.ForeignKey(Plano, default=None)


class Fatura(models.Model):
    iugu_id = models.CharField(max_length=32, null=True, blank=True, help_text='Id do fatura')
    data_pagamento = models.DateField(null=True, blank=True)
    assinatura = models.ForeignKey(Assinatura, default=None)

    def __unicode__(self):
        return unicode(self.plano.nome)
