<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xls="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>
  <xsl:param name="fileName" select="''"/>
  <xsl:template name="round">
    <xsl:param name="value"/>
    <xsl:value-of select="substring-before($value, '.')"/>
  </xsl:template>
  <xsl:template name="imoveis" match="indicadordeimoveis">
    <ListingDataFeed xmlns="http://www.vivareal.com/schemas/1.0/VRSync" xsi:schemaLocation="http://www.vivareal.com/schemas/1.0/VRSync http://xml.vivareal.com/vrsync.xsd">
      <Header>
        <Provider>CASASOFT</Provider>
        <Email>casasoft@casasoft.net.br</Email>
      </Header>
      <Listings>
        <xsl:for-each select="imovel">
          <xsl:call-template name="Listing"/>
        </xsl:for-each>
      </Listings>
    </ListingDataFeed>
  </xsl:template>
  <xsl:template name="Listing" match="fichaimoveis/imovel">
    <Listing xmlns="http://www.vivareal.com/schemas/1.0/VRSync">
      <xsl:if test="referencia != 0 and referencia != ''">
        <ListingID>
          <xsl:value-of select="referencia"/>
        </ListingID>
      </xsl:if>
      <xsl:if test="referencia != 0 and referencia != ''">
        <Title>
          <xsl:value-of select="concat(tipoimovel, ' ', bairro)"/>
        </Title>
      </xsl:if>
      <TransactionType>
        <xsl:if test="contains(imovelpara, 'L')">For Rent</xsl:if>
        <xsl:if test="contains(imovelpara, 'V')">For Sale</xsl:if>
      </TransactionType>
      <Location>
        <Country abbreviation="BR">BR</Country>
        <State>
          <xsl:attribute name="abbreviation"><xsl:value-of select="uf"/></xsl:attribute>
          <xsl:choose>
            <xsl:when test="uf = 'PR'">
              <xsl:text>Paraná</xsl:text>
            </xsl:when>
            <xsl:when test="uf = 'PE'">
              <xsl:text>Pernambuco</xsl:text>
            </xsl:when>
            <xsl:when test="uf = 'SC'">
              <xsl:text>Santa Catarina</xsl:text>
            </xsl:when>
            <xsl:when test="uf = 'SP'">
              <xsl:text>São Paulo</xsl:text>
            </xsl:when>
          </xsl:choose>
        </State>
        <City>
          <xsl:value-of select="cidade"/>
        </City>
        <Neighborhood>
          <xsl:value-of select="bairro"/>
        </Neighborhood>
        <Address>
		<xsl:choose>
			<xsl:when test="endereco/visivel = 'S'">
				<xsl:attribute name="publiclyVisible"><xsl:text>true</xsl:text></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="publiclyVisible"><xsl:text>false</xsl:text></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xls:choose>
			<xsl:when test="endereco/numero != ''">
				<xls:value-of select="concat(endereco/logradouro, ', ', endereco/numero)" />
			</xsl:when>
			<xsl:otherwise>
				<xls:value-of select="endereco/logradouro" />
			</xsl:otherwise>
		</xls:choose>
        </Address>
        <PostalCode>
          <xsl:value-of select="cep"/>
        </PostalCode>
      </Location>
      <Details>
        <Description>
            <xsl:if test="observacao != ''">
                <xsl:value-of select="observacao" />
            </xsl:if>
            <xsl:if test="observacao = ''">
                <xsl:value-of select="concat(tipoimovel, ' ', bairro)" />
            </xsl:if>
        </Description>
        <xsl:choose>
          <xsl:when test="imovelpara = 'V'">
            <xsl:if test="valortotal != '0.00' and valortotal != '0' and valortotal != ''">
            <ListPrice currency="BRL">
              <xsl:call-template name="round">
                <xsl:with-param name="value" select="valortotal"/>
              </xsl:call-template>
            </ListPrice>
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="valorlocacao != '0.00' and valorlocacao != '0' and valorlocacao != ''">
              <RentalPrice currency="BRL">
                <xsl:call-template name="round">
                  <xsl:with-param name="value" select="valorlocacao"/>
                </xsl:call-template>
              </RentalPrice>
            </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="valorcondominio != '0.00' and valorcondominio != '0' and valorcondominio != ''">
          <PropertyAdministrationFee currency="BRL">
            <xsl:call-template name="round">
              <xsl:with-param name="value" select="valorcondominio"/>
            </xsl:call-template>
          </PropertyAdministrationFee>
        </xsl:if>
        <PropertyType>
          <xsl:value-of select="tipoimovel"/>
        </PropertyType>
        <xsl:if test="areatotal != '0.00' and areatotal != '0' and areatotal != ''">
          <LotArea unit="square metres">
            <xsl:call-template name="round">
              <xsl:with-param name="value" select="areatotal"/>
            </xsl:call-template>
          </LotArea>
        </xsl:if>
        <xsl:if test="areautil != '0.00' and areautil != '0' and areautil != ''">
          <ConstructedArea unit="square metres">
            <xsl:call-template name="round">
              <xsl:with-param name="value" select="areautil"/>
            </xsl:call-template>
          </ConstructedArea>
        </xsl:if>
        <xsl:if test="caracteristicaspadrao/quarto != '0' and caracteristicaspadrao/quarto != ''">
          <Bedrooms>
            <xsl:value-of select="caracteristicaspadrao/quarto"/>
          </Bedrooms>
        </xsl:if>
        <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'BWC')]">
          <Bathrooms>
	        <xsl:choose>
				<xsl:when test="caracteristicasadicionais/caracteristica[contains(@nome, 'BWC')] = 'Sim' or 
				caracteristicasadicionais/caracteristica[contains(@nome, 'BWC')] = 'sim' or
				caracteristicasadicionais/caracteristica[contains(@nome, 'BWC')] = 'C/ BWC'">
					<xsl:text>1</xsl:text>
				</xsl:when>
				<xsl:when test="caracteristicasadicionais/caracteristica[contains(@nome, 'BWC')] = 'Nao'">
					<xsl:text>0</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="caracteristicasadicionais/caracteristica[contains(@nome, 'BWC')]"/>
				</xsl:otherwise>
			</xsl:choose>
          </Bathrooms>
        </xsl:if>
        <xsl:if test="caracteristicaspadrao/suite != '0' and caracteristicaspadrao/suite != ''">
          <Suites>
            <xsl:value-of select="caracteristicaspadrao/suite"/>
          </Suites>
        </xsl:if>
        <xsl:if test="caracteristicaspadrao/garagem != 'Sim' and caracteristicaspadrao/garagem != 'Não'">
          <Garage type="Parking Space">
            <xsl:value-of select="substring-before(caracteristicaspadrao/garagem, ' vaga')"/>
          </Garage>
        </xsl:if>
        <xsl:if test="endereco/numero != ''">
          <UnitNumber><xsl:value-of select="endereco/numero"/></UnitNumber>
        </xsl:if>
        <Features>
          <!-- Sistema de alarme                Alarm System -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Alarm')]">
            <Feature>Alarm System</Feature>
          </xsl:if>
          <!-- Aquecimento - Heating -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Aquecimento')]">
            <Feature>Heating</Feature>
          </xsl:if>
          <!-- Ar condicionado - Air Conditioning -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Ar Cond')]">
            <Feature>Air Conditioning</Feature>
          </xsl:if>          
          <!-- Area Jardim                  Garden  -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'jardim')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Jardim')] or caracteristicasadicionais/caracteristica[contains(@nome, 'JARDIM')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Jardins')]">
            <Feature>Garden</Feature>
          </xsl:if>
          <!-- Churrasqueira                  BBQ   -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Churr')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Churasq')]">
            <Feature>BBQ</Feature>
          </xsl:if>
          <!-- Circuito de segurança              TV Security  -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Interno TV')]">
            <Feature>TV Security</Feature>
          </xsl:if>
          <!-- Conjunto cerrado               Gated Community  - Condomínio fechado  -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Cond. fechado')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Cond. Fechado')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Condominio')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Condomínio')]">
            <Feature>Gated Community</Feature>
          </xsl:if>
          <!-- Cozinha                      Kitchen  -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Cozinha')]">
            <Feature>Kitchen</Feature>
          </xsl:if>
          <!-- Elevador                   Elevator  -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'levador')]">
            <Feature>Elevator</Feature>
          </xsl:if>
          <!-- Garagem                      RV Parking  -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Garagem')] or caracteristicasadicionais/caracteristica[contains(@nome, 'garage')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Garagens')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Gar.Automática')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Gar. Automatica')]">
            <Feature>RV Parking</Feature>
          </xsl:if>
          <!-- Lareira                      Fireplace  -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Lareira')]">
            <Feature>Fireplace</Feature>
          </xsl:if>
          <!-- Interfone                    Intercom    -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Lareira')]">
            <Feature>Intercom</Feature>
          </xsl:if>
          <!-- Piscina                      Pool    -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Piscina')]">
            <Feature>Pool</Feature>
          </xsl:if>
          <!-- Playground                   Playground    -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Play Gorund')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Playgrond')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Playground')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Play ground')] or caracteristicasadicionais/caracteristica[contains(@nome, 'PlayGround')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Play Ground')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Play-Ground')]">
            <Feature>Playground</Feature>
          </xsl:if>
         <!-- Quadra de tênis                 Tennis court -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Quadra de Tênis')] or caracteristicasadicionais/caracteristica[contains(@nome, 'Quadra de Tenis')]">
            <Feature>Tennis court</Feature>
          </xsl:if>
         <!-- Quintal                     Backyard -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'uintal')]">
            <Feature>Backyard</Feature>
          </xsl:if>
         <!-- Sacada                      Balcony/Terrace -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Sacada')]">
            <Feature>Balcony/Terrace</Feature>
          </xsl:if>
         <!-- Sacada                      Balcony/Terrace -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Sacada')]">
            <Feature>Balcony/Terrace</Feature>
          </xsl:if>
         <!-- Segurança 24 horas                24 Hour Security -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Seg. 24 hs')]">
            <Feature>24 Hour Security</Feature>
          </xsl:if>
         <!-- Segurança 24 horas                24 Hour Security -->
          <xsl:if test="caracteristicasadicionais/caracteristica[contains(@nome, 'Seg. 24 hs')]">
            <Feature>24 Hour Security</Feature>
          </xsl:if>
        </Features>
      </Details>
      <xsl:if test="galeriaimagens/fotos/foto != ''">
        <Media>
          <xsl:for-each select="galeriaimagens/fotos/foto">
          	<xsl:sort select="principal" order="descending"/>
            <Item medium="image">
              <xsl:if test="principal = 'S'">
                <xsl:attribute name="primary">true</xsl:attribute>
              </xsl:if>
              <xsl:if test="contains(urlarquivo, '/')">
                <xsl:value-of select="urlarquivo"/>
              </xsl:if>
            </Item>
          </xsl:for-each>
        </Media>
      </xsl:if>
      <ContactInfo>
        <Email>
          <xsl:if test="/indicadordeimoveis/dadosimobiliaria/email != '0' and /indicadordeimoveis/dadosimobiliaria/email != ''">
            <xsl:value-of select="/indicadordeimoveis/dadosimobiliaria/email"/>
          </xsl:if>
        </Email>
        <Name>
          <xsl:if test="/indicadordeimoveis/dadosimobiliaria/nome != '0' and /indicadordeimoveis/dadosimobiliaria/nome != ''">
            <xsl:value-of select="/indicadordeimoveis/dadosimobiliaria/nome"/>
          </xsl:if>
        </Name>
      </ContactInfo>
      <xsl:if test="destaque = '1'">
        <Featured>true</Featured>
      </xsl:if>
    </Listing>
  </xsl:template>
</xsl:stylesheet>
