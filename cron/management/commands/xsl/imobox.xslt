<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Imobox" match="/">
        <property>
            <referer><xsl:value-of select="ref | codigo"/></referer>
            <title><xsl:value-of select="titulo"/></title>
            <category><xsl:value-of select="tipo"/></category>

            <xsl:choose>
                <xsl:when test="operacao">
                    <transaction><xsl:value-of select="operacao"/></transaction>
                </xsl:when>
                <xsl:when test="negociacao = 'Venda' or negociacao = 'Lançamento'">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:when test="negociacao = 'Locação'">
                    <transaction>L</transaction>
                </xsl:when>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="negociacao = 'Venda' or negociacao = 'Lançamento' or operacao = 'v'">
                    <sale_price>
                        <xsl:choose>
                            <xsl:when test="contains(valor, ',00')">
                                <xsl:value-of select="translate(translate(substring-before(valor, ',00'), '.', ''), 'R$', '')"/>
                            </xsl:when>
                            <xsl:when test="contains(valor, ',')">
                                <xsl:value-of select="translate(translate(translate(valor,'.',''), ',', '.'), 'R$', '')"/>
                            </xsl:when>
                            <xsl:when test="substring(valor, (string-length(valor) - string-length('.00')) + 1) = '.00'">
                                <xsl:value-of select="translate(translate(substring-before(valor, '.00'),'.', ''), 'R$', '')"/>
                            </xsl:when>
                            <xsl:when test="contains(substring(valor, (string-length(valor) - 2)), '.')">
                                <xsl:value-of select="translate(substring-before(valor, substring(valor, (string-length(valor) - 2))),'.','')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(translate(valor, '.', ''), 'R$', '')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </sale_price>
                </xsl:when>
                <xsl:when test="negociacao = 'Locação' or operacao = 'l'">
                    <rent_price>
                        <xsl:choose>
                            <xsl:when test="contains(valor, ',00')">
                                <xsl:value-of select="translate(translate(substring-before(valor, ',00'), '.', ''), 'R$', '')"/>
                            </xsl:when>
                            <xsl:when test="contains(valor, ',')">
                                <xsl:value-of select="translate(translate(translate(valor,'.',''), ',', '.'), 'R$', '')"/>
                            </xsl:when>
                            <xsl:when test="substring(valor, (string-length(valor) - string-length('.00')) + 1) = '.00'">
                                <xsl:value-of select="translate(translate(substring-before(valor, '.00'),'.', ''), 'R$', '')"/>
                            </xsl:when>
                            <xsl:when test="contains(substring(valor, (string-length(valor) - 2)), '.')">
                                <xsl:value-of select="translate(substring-before(valor, substring(valor, (string-length(valor) - 2))),'.','')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(translate(valor, '.', ''), 'R$', '')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </rent_price>
                </xsl:when>
            </xsl:choose>


            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoCondominio, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoCondominio, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(PrecoCondominio, ',')">
                        <xsl:value-of select="translate(translate(translate(PrecoCondominio,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(PrecoCondominio, (string-length(PrecoCondominio) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(PrecoCondominio, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(PrecoCondominio, (string-length(PrecoCondominio) - 2)), '.')">
                        <xsl:value-of select="translate(substring-before(PrecoCondominio, substring(PrecoCondominio, (string-length(PrecoCondominio) - 2))),'.','')"/>
                    </xsl:when>
                    <xsl:when test="contains(valorCondominio, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(valorCondominio, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(valorCondominio, ',')">
                        <xsl:value-of select="translate(translate(translate(valorCondominio,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(valorCondominio, (string-length(valorCondominio) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(valorCondominio, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(valorCondominio, (string-length(valorCondominio) - 2)), '.')">
                        <xsl:value-of select="translate(substring-before(valorCondominio, substring(valorCondominio, (string-length(valorCondominio) - 2))),'.','')"/>
                    </xsl:when>
                    <xsl:when test="PrecoCondominio">
                        <xsl:value-of select="PrecoCondominio"/>
                    </xsl:when>
                    <xsl:when test="valorCondominio">
                        <xsl:value-of select="valorCondominio"/>
                    </xsl:when>
                </xsl:choose>
            </condo_price>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(areaPrivativa, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(areaPrivativa, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(areaPrivativa, ',')">
                        <xsl:value-of select="translate(translate(areaPrivativa,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(areaPrivativa, ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(areaTotal, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(areaTotal, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(areaTotal, ',')">
                        <xsl:value-of select="translate(translate(areaTotal,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(translate(areaTotal,'.',''), ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="quartos"/></bedroom>
            <bathroom><xsl:value-of select="banheiros"/></bathroom>
            <suite><xsl:value-of select="suites"/></suite>
            <garage><xsl:value-of select="vagas"/></garage>

            <description>&lt;![CDATA[
                <xsl:choose>
                    <xsl:when test="descricao">
                        <xsl:value-of select="descricao"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="observacao | MemorialDescritivo"/>
                    </xsl:otherwise>
                </xsl:choose>
                ]]&gt;
            </description>

            <xsl:choose>
                <xsl:when test="Pais">
                    <country><xsl:value-of select="Pais"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>


            <state><xsl:value-of select="uf"/></state>
            <city><xsl:value-of select="cidade"/></city>
            <neighborhood><xsl:value-of select="bairro"/></neighborhood>

            <latitude><xsl:value-of select="translate(latitude, 'X', '')"/></latitude>
            <longitude><xsl:value-of select="translate(longitude, 'Y', '')"/></longitude>

            <zipcode><xsl:value-of select="CEP"/></zipcode>
            <address><xsl:value-of select="Endereco | rua"/></address>
            <street_number><xsl:value-of select="Numero | numero"/></street_number>
            <complement><xsl:value-of select="Complemento | ComplementoEndereco | complemento"/></complement>

            <photos>
                <xsl:for-each select="fotos/foto">
                    <photo>
                        <url><xsl:value-of select="text()"/></url>
                    </photo>
                </xsl:for-each>
                <xsl:for-each select="url_imagem">
                    <photo>
                        <url><xsl:value-of select="text()"/></url>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
