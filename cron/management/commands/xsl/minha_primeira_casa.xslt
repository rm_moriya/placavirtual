<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="MinhaPrimeiraCasa" match="/">
        <property>
            <referer><xsl:value-of select="referencia"/></referer>
            <category><xsl:value-of select="tipo"/></category>
            <transaction><xsl:value-of select="transacao"/></transaction>
            <goal><xsl:value-of select="finalidade"/></goal>

            <xsl:choose>
                <xsl:when test="finalidade='V'">
                    <sale_price>
                        <xsl:choose>
                            <xsl:when test="contains(valor, ',00')">
                                <xsl:value-of select="translate(substring-before(valor, ',00'), '.', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(valor, ',', '.')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </sale_price>
                </xsl:when>
                <xsl:when test="finalidade='A'">
                    <rent_price>
                        <xsl:choose>
                            <xsl:when test="contains(valor, ',00')">
                                <xsl:value-of select="translate(substring-before(valor, ',00'), '.', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(valor, ',', '.')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </rent_price>
                </xsl:when>
            </xsl:choose>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(area_util, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(area_util, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(area_util, ',')">
                        <xsl:value-of select="translate(translate(area_util,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(area_util, ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(area_total, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(area_total, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(area_total, ',')">
                        <xsl:value-of select="translate(translate(area_total,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(area_total, ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="quartos"/></bedroom>
            <bathroom><xsl:value-of select="banheiros"/></bathroom>
            <suite><xsl:value-of select="suites"/></suite>
            <garage><xsl:value-of select="garagem"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="descricao"/>]]&gt;</description>

             <xsl:choose>
                <xsl:when test="pais">
                    <country><xsl:value-of select="pais"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>
            <state><xsl:value-of select="estado"/></state>
            <city><xsl:value-of select="cidade"/></city>
            <zone><xsl:value-of select="zona"/></zone>
            <neighborhood><xsl:value-of select="bairro"/></neighborhood>

            <zipcode><xsl:value-of select="cep"/></zipcode>
            <address><xsl:value-of select="endereco"/></address>
            <street_number><xsl:value-of select="numero"/></street_number>
            <complement><xsl:value-of select="complemento"/></complement>

            <photos>
                <photo><url><xsl:value-of select="foto_principal"/></url></photo>
                <xsl:for-each select="fotos/foto">
                    <photo><url><xsl:value-of select="text()"/></url></photo>
                </xsl:for-each>
            </photos>
            <videos>
                <xsl:for-each select="Media/Item">
                    <xsl:choose>
                        <xsl:when test="@medium='video'">
                            <video><xsl:value-of select="text()"/></video>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </videos>
        </property>
    </xsl:template>
</xsl:stylesheet>
