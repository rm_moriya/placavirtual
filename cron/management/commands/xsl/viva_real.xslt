<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="VivaRealImovel" match="/">
        <property>
            <referer><xsl:value-of select="ListingID"/></referer>
            <title>&lt;![CDATA[<xsl:value-of select="Title"/>]]&gt;</title>
            <category><xsl:value-of select="Details/PropertyType"/></category>

            <xsl:choose>
                <xsl:when test="TransactionType = 'For Sale' or TransactionType = 'For sale'">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:when test="TransactionType = 'For Rent' or TransactionType = 'For rent'">
                    <xsl:choose>
                        <xsl:when test="Details/RentalPrice/@period='Daily'">
                            <transaction>T</transaction>
                        </xsl:when>
                        <xsl:otherwise>
                            <transaction>L</transaction>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="TransactionType = 'Sale/Rent'">
                    <xsl:choose>
                        <xsl:when test="Details/RentalPrice/@period='Daily'">
                            <transaction>VT</transaction>
                        </xsl:when>
                        <xsl:otherwise>
                            <transaction>VL</transaction>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="contains(Details/PropertyType, 'RESIDENCIAL') or contains(Details/PropertyType, 'Residencial')">
                    <goal>R</goal>
                </xsl:when>
                <xsl:when test="contains(Details/PropertyType, 'COMERCIAL') or contains(Details/PropertyType, 'Comercial')">
                    <goal>C</goal>
                </xsl:when>
                <xsl:otherwise>
                    <goal>R</goal>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="TransactionType = 'For Sale' or TransactionType = 'For sale'">
                    <sale_price>
                        <xsl:choose>
                            <xsl:when test="contains(Details/ListPrice, ',00')">
                                <xsl:value-of select="translate(substring-before(Details/ListPrice, ',00'), '.', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(translate(Details/ListPrice, ',', '.'), 'X', '')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </sale_price>
                </xsl:when>
                <xsl:when test="TransactionType = 'For Rent' or TransactionType = 'For rent'">
                    <xsl:choose>
                        <xsl:when test="Details/RentalPrice/@period='Daily'">
                            <season_price>
                                <xsl:choose>
                                    <xsl:when test="contains(Details/RentalPrice, ',00')">
                                        <xsl:value-of select="translate(substring-before(Details/RentalPrice, ',00'), '.', '')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="translate(translate(Details/RentalPrice, ',', '.'), 'X', '')"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </season_price>
                        </xsl:when>
                        <xsl:otherwise>
                            <rent_price>
                                <xsl:choose>
                                    <xsl:when test="contains(Details/RentalPrice, ',00')">
                                        <xsl:value-of select="translate(substring-before(Details/RentalPrice, ',00'), '.', '')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="translate(translate(Details/RentalPrice, ',', '.'), 'X', '')"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </rent_price>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="TransactionType = 'Sale/Rent'">
                    <sale_price>
                        <xsl:choose>
                            <xsl:when test="contains(Details/ListPrice, ',00')">
                                <xsl:value-of select="translate(substring-before(Details/ListPrice, ',00'), '.', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(translate(Details/ListPrice, ',', '.'), 'X', '')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </sale_price>
                    <xsl:choose>
                        <xsl:when test="Details/RentalPrice/@period='Daily'">
                            <season_price>
                                <xsl:choose>
                                    <xsl:when test="contains(Details/RentalPrice, ',00')">
                                        <xsl:value-of select="translate(substring-before(Details/RentalPrice, ',00'), '.', '')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="translate(translate(Details/RentalPrice, ',', '.'), 'X', '')"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </season_price>
                        </xsl:when>
                        <xsl:otherwise>
                            <rent_price>
                                <xsl:choose>
                                    <xsl:when test="contains(Details/RentalPrice, ',00')">
                                        <xsl:value-of select="translate(substring-before(Details/RentalPrice, ',00'), '.', '')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="translate(translate(Details/RentalPrice, ',', '.'), 'X', '')"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </rent_price>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(Details/ConstructedArea, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/ConstructedArea, ',00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(Details/ConstructedArea, '. 00m².00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/ConstructedArea, '. 00m².00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(Details/ConstructedArea, '.00m².00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/ConstructedArea, '.00m².00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(Details/LivingArea, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/LivingArea, ',00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(Details/LivingArea, '. 00m².00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/LivingArea, '. 00m².00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(Details/LivingArea, '.00m².00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/LivingArea, '.00m².00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="Details/LivingArea">
                        <xsl:value-of select="translate(translate(Details/LivingArea, ',', '.'),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="Details/ConstructedArea">
                        <xsl:value-of select="translate(translate(Details/ConstructedArea, ',', '.'),'M²','')"/>
                    </xsl:when>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(Details/LotArea, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/LotArea, ',00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(Details/LotArea, '. 00m².00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/LotArea, '. 00m².00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(Details/LotArea, '.00m².00')">
                        <xsl:value-of select="translate(translate(substring-before(Details/LotArea, '.00m².00'), '.', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(Details/LotArea, ',', '.'),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(Details/PropertyAdministrationFee, ',00')">
                        <xsl:value-of select="translate(substring-before(Details/PropertyAdministrationFee, ',00'), '.', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(Details/PropertyAdministrationFee, ',', '.')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condo_price>

            <bedroom><xsl:value-of select="translate(Details/Bedrooms,'-- Não informado --','')"/></bedroom>
            <bathroom><xsl:value-of select="Details/Bathrooms"/></bathroom>
            <suite><xsl:value-of select="Details/Suites"/></suite>
            <garage><xsl:value-of select="Details/Garage"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="Details/Description"/>]]&gt;</description>
            <xsl:choose>
                <xsl:when test="Location/Country">
                    <country><xsl:value-of select="Location/Country"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>
            <state><xsl:value-of select="Location/State"/></state>
            <city><xsl:value-of select="Location/City"/></city>
            <zone><xsl:value-of select="Location/Zone"/></zone>
            <neighborhood><xsl:value-of select="Location/Neighborhood"/></neighborhood>

            <address><xsl:value-of select="Location/Address"/></address>
            <latitude><xsl:value-of select="Location/Latitude"/></latitude>
            <longitude><xsl:value-of select="Location/Longitude"/></longitude>
            <photos>
                <xsl:for-each select="Media/Item">
                    <xsl:choose>
                        <xsl:when test="@medium='image'">
                            <photo>
                                <url><xsl:value-of select="text()"/></url>
                            </photo>
                        </xsl:when>
                        <xsl:otherwise>
                            <photo>
                                <url><xsl:value-of select="text()"/></url>
                            </photo>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </photos>
            <videos>
                <xsl:for-each select="Media/Item">
                    <xsl:choose>
                        <xsl:when test="@medium='video'">
                            <video>
                                <url><xsl:value-of select="text()"/></url>
                            </video>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </videos>

            <characteristics>
                <xsl:for-each select="Features/Feature">
                    <characteristic><xsl:value-of select="text()"/></characteristic>
                </xsl:for-each>
            </characteristics>

        </property>
    </xsl:template>
</xsl:stylesheet>
