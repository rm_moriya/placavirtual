<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Casasoft" match="/">
        <property>
            <referer><xsl:value-of select="referencia"/></referer>
            <category><xsl:value-of select="tipoimovel"/></category>
            <transaction><xsl:value-of select="imovelpara"/></transaction>
            <goal><xsl:value-of select="finalidade"/></goal>

            <xsl:if test="imovelpara = 'V'">
                <sale_price>
                    <xsl:choose>
                        <xsl:when test="contains(valortotal, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(valortotal, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(valortotal, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </sale_price>
            </xsl:if>

            <xsl:if test="imovelpara = 'L'">
                <rent_price>
                    <xsl:choose>
                        <xsl:when test="contains(valortotal, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(valortotal, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(valortotal, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </rent_price>
            </xsl:if>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(areautil, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(areautil, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(areautil, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(areatotal, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(areatotal, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(areatotal, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(valorcondominio, ',00')">
                        <xsl:value-of select="translate(substring-before(valorcondominio, ',00'), '.', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(valorcondominio, ',', '.')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condo_price>

            <bedroom><xsl:value-of select="caracteristicaspadrao/quarto"/></bedroom>
            <suite><xsl:value-of select="caracteristicasadicionais/caracteristica[@nome='Suíte']"/></suite>
            <garage><xsl:value-of select="translate(caracteristicaspadrao/garagem,'vaga','')"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="observacao"/>]]&gt;</description>

            <country><xsl:value-of select="pais"/></country>
            <state><xsl:value-of select="uf"/></state>
            <city><xsl:value-of select="cidade"/></city>
            <zone><xsl:value-of select="zona"/></zone>
            <neighborhood><xsl:value-of select="bairro"/></neighborhood>

            <latitude><xsl:value-of select="latitude"/></latitude>
            <longitude><xsl:value-of select="longitude"/></longitude>

            <zipcode><xsl:value-of select="cep"/></zipcode>
            <address><xsl:value-of select="endereco/logradouro"/></address>
            <street_number><xsl:value-of select="endereco/numero"/></street_number>
            <complement><xsl:value-of select="endereco/complemento"/></complement>

            <photos>
                <xsl:for-each select="galeriaimagens/fotos/foto">
                    <photo>
                        <url><xsl:value-of select="urlarquivo"/></url>
                        <order><xsl:value-of select="ordem"/></order>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
