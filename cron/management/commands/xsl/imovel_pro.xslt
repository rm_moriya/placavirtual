<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="ImovelPro" match="/">
        <property>
            <referer><xsl:value-of select="referencia"/></referer>
            <category><xsl:value-of select="caracteristicas/tipo_imovel"/></category>
            <xsl:choose>
                <xsl:when test="negociacao/venda and negociacao/locacao">
                    <transaction>VL</transaction>
                </xsl:when>
                <xsl:when test="negociacao/venda">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:when test="negociacao/locacao">
                    <transaction>L</transaction>
                </xsl:when>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="contains(title, 'Comercial') or contains(title, 'Comercial') or contains(descricao/super_comentario,'Comercial')">
                    <goal>C</goal>
                </xsl:when>
                <xsl:otherwise>
                    <goal>R</goal>
                </xsl:otherwise>
            </xsl:choose>

            <sale_price>
                <xsl:choose>
                    <xsl:when test="contains(negociacao/venda/preco, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(negociacao/venda/preco, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(negociacao/venda/preco, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </sale_price>
            <rent_price>
                <xsl:choose>
                    <xsl:when test="contains(negociacao/locacao/preco, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(negociacao/locacao/preco, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(negociacao/locacao/preco, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </rent_price>
            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(taxas/valor_condominio, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(taxas/valor_condominio, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(taxas/valor_condominio, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condo_price>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(medidas/area_privativa, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(medidas/area_privativa, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(medidas/area_privativa, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(medidas/area_total, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(medidas/area_total, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(medidas/area_total, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="ambientes/dormitorios"/></bedroom>
            <bathroom><xsl:value-of select="ambientes/banheiros"/></bathroom>
            <suite><xsl:value-of select="ambientes/sendo_suites"/></suite>
            <garage><xsl:value-of select="ambientes/garagens"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="descricao/descricao_externa"/>]]&gt;</description>

            <xsl:choose>
                <xsl:when test="endereco/pais">
                    <country><xsl:value-of select="endereco/pais"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>
            <state><xsl:value-of select="endereco/estado"/></state>
            <city><xsl:value-of select="endereco/cidade"/></city>
            <zone><xsl:value-of select="endereco/zona"/></zone>
            <neighborhood><xsl:value-of select="endereco/bairro"/></neighborhood>

            <latitude><xsl:value-of select="google_maps/latitude"/></latitude>
            <longitude><xsl:value-of select="google_maps/longitude"/></longitude>

            <zipcode><xsl:value-of select="endereco/cep"/></zipcode>
            <address><xsl:value-of select="endereco/logradouro"/></address>
            <street_number><xsl:value-of select="endereco/numero"/></street_number>
            <complement><xsl:value-of select="cendereco/complemento"/></complement>

            <photos>
                <xsl:for-each select="fotos/foto">
                    <photo>
                        <url><xsl:value-of select="url"/></url>
                    </photo>
                </xsl:for-each>
            </photos>

            <characteristics>
                <xsl:if test="churrasqueiras > 0"><characteristic>Churrasqueira</characteristic></xsl:if>
                <xsl:if test="piscinas > 0"><characteristic>Piscinas</characteristic></xsl:if>
                <xsl:for-each select="caracteristicas_adicionais/item">
                    <characteristic><xsl:value-of select="text()"/></characteristic>
                </xsl:for-each>
            </characteristics>

        </property>
    </xsl:template>
</xsl:stylesheet>
