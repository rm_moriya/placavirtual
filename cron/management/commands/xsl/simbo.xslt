<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Simbo" match="/">
        <property>
            <referer><xsl:value-of select="Ref"/></referer>

            <subcategory><xsl:value-of select="SubType"/></subcategory>
            <category><xsl:value-of select="Type"/></category>

            <xsl:choose>
                <xsl:when test="AvailableToSell = 'true' and AvailableToRent = 'true'">
                    <transaction>VL</transaction>
                </xsl:when>
                <xsl:when test="AvailableToSell = 'true'">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:when test="AvailableToRent = 'true'">
                    <transaction>L</transaction>
                </xsl:when>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="Type = 'Comercial'">
                    <goal>C</goal>
                </xsl:when>
                <xsl:otherwise>
                    <goal>R</goal>
                </xsl:otherwise>
            </xsl:choose>

            <sale_price>
                <xsl:choose>
                    <xsl:when test="contains(SellPrice, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(SellPrice, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(SellPrice, ',')">
                        <xsl:value-of select="translate(translate(translate(SellPrice,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(SellPrice, (string-length(SellPrice) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(SellPrice, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(SellPrice, (string-length(SellPrice) - 1)), '.')">
                        <xsl:value-of select="translate(substring-before(SellPrice, substring(SellPrice, (string-length(SellPrice) - 1))),'.','')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(SellPrice, '.', ''), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </sale_price>

            <rent_price>
                <xsl:choose>
                    <xsl:when test="contains(RentPrice, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(RentPrice, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(RentPrice, ',')">
                        <xsl:value-of select="translate(translate(translate(RentPrice,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(RentPrice, (string-length(RentPrice) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(RentPrice, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(RentPrice, (string-length(RentPrice) - 1)), '.')">
                        <xsl:value-of select="translate(substring-before(RentPrice, substring(RentPrice, (string-length(RentPrice) - 1))),'.','')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(RentPrice, '.', ''), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </rent_price>

            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(CondominiumPrice, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(CondominiumPrice, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(CondominiumPrice, ',')">
                        <xsl:value-of select="translate(translate(translate(CondominiumPrice,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(CondominiumPrice, (string-length(CondominiumPrice) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(CondominiumPrice, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(CondominiumPrice, (string-length(CondominiumPrice) - 1)), '.')">
                        <xsl:value-of select="translate(substring-before(CondominiumPrice, substring(CondominiumPrice, (string-length(CondominiumPrice) - 1))),'.','')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(CondominiumPrice, '.', ''), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condo_price>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(UtilArea, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(UtilArea, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(UtilArea, ',')">
                        <xsl:value-of select="translate(translate(UtilArea,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(UtilArea, ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(TotalArea, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(TotalArea, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(TotalArea, ',')">
                        <xsl:value-of select="translate(translate(TotalArea,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(translate(TotalArea,'.',''), ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="Rooms"/></bedroom>
            <bathroom><xsl:value-of select="Restrooms"/></bathroom>
            <suite><xsl:value-of select="Suites"/></suite>
            <garage><xsl:value-of select="Parking"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="Info"/>]]&gt;</description>

            <xsl:choose>
                <xsl:when test="Location/Country">
                    <country><xsl:value-of select="Location/Country"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>


            <state><xsl:value-of select="Location/State"/></state>
            <city><xsl:value-of select="Location/City"/></city>
            <neighborhood><xsl:value-of select="Location/Neighborhood"/></neighborhood>

            <latitude><xsl:value-of select="translate(Location/Latitude, 'X', '')"/></latitude>
            <longitude><xsl:value-of select="translate(Location/Longitude, 'Y', '')"/></longitude>

            <zipcode><xsl:value-of select="Location/PostalCode"/></zipcode>
            <address><xsl:value-of select="Location/Address"/></address>
            <street_number><xsl:value-of select="translate(translate(translate(Location/Number,'S/N',''),'xxx',''),'.','')"/></street_number>
            <complement><xsl:value-of select="Location/Complement"/></complement>

            <photos>
                <xsl:for-each select="Media/MediaItem">
                    <xsl:choose>
                        <xsl:when test="@type='image'">
                            <photo>
                                <url><xsl:value-of select="text()"/></url>
                            </photo>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </photos>

            <characteristics>
                <xsl:for-each select="Features/Feature">
                    <characteristic><xsl:value-of select="text()"/></characteristic>
                </xsl:for-each>
            </characteristics>

        </property>
    </xsl:template>
</xsl:stylesheet>
