<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Trovit" match="/">
        <property>
            <referer><xsl:value-of select="id"/></referer>
            <category><xsl:value-of select="property_type"/></category>
            <xsl:choose>
                <xsl:when test="type = 'For sale'">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:otherwise>
                    <transaction>L</transaction>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="contains(title, 'Comercial') or contains(title, 'Comercial')">
                    <goal>C</goal>
                </xsl:when>
                <xsl:otherwise>
                    <goal>R</goal>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:if test="type = 'For sale'">
                <sale_price>
                    <xsl:choose>
                        <xsl:when test="contains(price, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(price, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(price, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </sale_price>
            </xsl:if>

            <xsl:if test="transacao = 'For rent'">
                <rent_price>
                    <xsl:choose>
                        <xsl:when test="contains(price, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(price, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(price, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </rent_price>
            </xsl:if>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(floor_area, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(floor_area, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(floor_area, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(area_total, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(area_total, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(area_total, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="rooms"/></bedroom>
            <bathroom><xsl:value-of select="bathrooms"/></bathroom>
            <suite><xsl:value-of select="suites"/></suite>
            <garage><xsl:value-of select="parking"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="content"/>]]&gt;</description>

            <xsl:choose>
                <xsl:when test="pais">
                    <country><xsl:value-of select="pais"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>
            <state><xsl:value-of select="region"/></state>
            <city><xsl:value-of select="city"/></city>
            <zone><xsl:value-of select="zona"/></zone>
            <neighborhood><xsl:value-of select="city_area"/></neighborhood>
            <latitude><xsl:value-of select="latitude"/></latitude>
            <longitude><xsl:value-of select="longitude"/></longitude>

            <zipcode><xsl:value-of select="postcode"/></zipcode>
            <address><xsl:value-of select="endereco"/></address>
            <complement><xsl:value-of select="complemento"/></complement>

            <photos>
                <xsl:for-each select="pictures/picture">
                    <photo>
                        <url><xsl:value-of select="picture_url"/></url>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
