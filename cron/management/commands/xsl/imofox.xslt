<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Imofox" match="/">
        <property>
            <referer><xsl:value-of select="referencia"/></referer>
            <category><xsl:value-of select="tipoimovel"/></category>
            <transaction>
                <xsl:choose>
                    <xsl:when test="finalidade = 'venda'">V</xsl:when>
                    <xsl:when test="finalidade = 'aluguel' or finalidade = 'locacao'">L</xsl:when>
                </xsl:choose>
            </transaction>

            <xsl:choose>
                <xsl:when test="finalidade = 'venda'">
                    <sale_price>
                        <xsl:choose>
                            <xsl:when test="contains(preco, ',00')">
                                <xsl:value-of select="translate(translate(substring-before(preco, ',00'), '.', ''), 'R$', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(translate(translate(preco, ',', '.'), 'R$', ''), '.', '')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </sale_price>
                </xsl:when>
                <xsl:when test="finalidade = 'aluguel' or finalidade = 'locacao'">
                    <rent_price>
                        <xsl:choose>
                            <xsl:when test="contains(preco, ',00')">
                                <xsl:value-of select="translate(translate(substring-before(preco, ',00'), '.', ''), 'R$', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(translate(translate(preco, ',', '.'), 'R$', ''), '.', '')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </rent_price>
                </xsl:when>
            </xsl:choose>


            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(area, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(area, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(area, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <bedroom><xsl:value-of select="dormitorios"/></bedroom>
            <bathroom><xsl:value-of select="banheiros"/></bathroom>
            <suite><xsl:value-of select="suites"/></suite>
            <garage><xsl:value-of select="vagas"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="descricao"/>]]&gt;</description>

            <country>
                <xsl:choose>
                    <xsl:when test="Pais">
                        <xsl:value-of select="Pais"/>
                    </xsl:when>
                    <xsl:otherwise>BR</xsl:otherwise>
                </xsl:choose>
            </country>
            <state><xsl:value-of select="estado"/></state>
            <city><xsl:value-of select="cidade"/></city>
            <neighborhood><xsl:value-of select="bairro"/></neighborhood>

            <latitude><xsl:value-of select="latitude"/></latitude>
            <longitude><xsl:value-of select="longitude"/></longitude>

            <zipcode><xsl:value-of select="cep"/></zipcode>
{#            <address><xsl:value-of select="endereco"/></address>#}
{#            <complement><xsl:value-of select="complemento"/></complement>#}

            <photos>
                <xsl:for-each select="fotos/foto">
                    <photo>
                        <url><xsl:value-of select="foto_url"/></url>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
