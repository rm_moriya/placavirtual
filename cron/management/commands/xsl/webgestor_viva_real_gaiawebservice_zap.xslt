<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="WebGestorVivaReal_GaiaWebService_Zap" match="/">
        <property>
            <referer><xsl:value-of select="RefWG | CodigoImovel"/></referer>
            <title><xsl:value-of select="TituloImovel"/></title>

            <subcategory><xsl:value-of select="SubTipoImovel"/></subcategory>
            <category><xsl:value-of select="TipoImovel"/></category>

            <xsl:choose>
                <xsl:when test="Transacao">
                    <transaction><xsl:value-of select="Transacao"/></transaction>
                </xsl:when>
                <xsl:when test="PrecoVenda != 0 and PrecoLocacao != 0 and PrecoVenda != '' and PrecoLocacao != ''">
                    <transaction>VL</transaction>
                </xsl:when>
                <xsl:when test="PrecoVenda != 0 and PrecoVenda != ''">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:when test="PrecoLocacao != 0 and PrecoLocacao != ''">
                    <transaction>L</transaction>
                </xsl:when>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="Finalidade = 'Residencial' or Finalidade = 'R'">
                    <goal>R</goal>
                </xsl:when>
                <xsl:when test="Finalidade = 'Comercial' or Finalidade = 'C'">
                    <goal>C</goal>
                </xsl:when>
                <xsl:when test="contains(SubTipoImovel, 'RESIDENCIAL') or contains(SubTipoImovel, 'Residencial')">
                    <goal>R</goal>
                </xsl:when>
                <xsl:when test="contains(SubTipoImovel, 'COMERCIAL') or contains(Details/PropertyType, 'Comercial')">
                    <goal>C</goal>
                </xsl:when>
                <xsl:otherwise>
                    <goal>R</goal>
                </xsl:otherwise>
            </xsl:choose>

            <sale_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoVenda, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoVenda, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(PrecoVenda, ',')">
                        <xsl:value-of select="translate(translate(translate(PrecoVenda,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(PrecoVenda, (string-length(PrecoVenda) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(PrecoVenda, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(PrecoVenda, (string-length(PrecoVenda) - 2)), '.')">
                        <xsl:value-of select="translate(substring-before(PrecoVenda, substring(PrecoVenda, (string-length(PrecoVenda) - 2))),'.','')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(PrecoVenda, '.', ''), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </sale_price>

            <rent_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoLocacao, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoLocacao, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(PrecoLocacao, ',')">
                        <xsl:value-of select="translate(translate(translate(PrecoLocacao,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(PrecoLocacao, (string-length(PrecoLocacao) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(PrecoLocacao, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(PrecoLocacao, (string-length(PrecoLocacao) - 2)), '.')">
                        <xsl:value-of select="translate(substring-before(PrecoLocacao, substring(PrecoLocacao, (string-length(PrecoLocacao) - 2))),'.','')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(PrecoLocacao, '.', ''), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </rent_price>

            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoCondominio, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoCondominio, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(PrecoCondominio, ',')">
                        <xsl:value-of select="translate(translate(translate(PrecoCondominio,'.',''), ',', '.'), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="substring(PrecoCondominio, (string-length(PrecoCondominio) - string-length('.00')) + 1) = '.00'">
                        <xsl:value-of select="translate(translate(substring-before(PrecoCondominio, '.00'),'.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(substring(PrecoCondominio, (string-length(PrecoCondominio) - 2)), '.')">
                        <xsl:value-of select="translate(substring-before(PrecoCondominio, substring(PrecoCondominio, (string-length(PrecoCondominio) - 2))),'.','')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(PrecoCondominio, '.', ''), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condo_price>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(AreaUtil, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(AreaUtil, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(AreaUtil, ',')">
                        <xsl:value-of select="translate(translate(AreaUtil,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(AreaUtil, ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(AreaTotal, ',00')">
                        <xsl:value-of select="translate(translate(translate(substring-before(AreaTotal, ',00'), '.', ''), 'm²', ''),'M²','')"/>
                    </xsl:when>
                    <xsl:when test="contains(AreaTotal, ',')">
                        <xsl:value-of select="translate(translate(AreaTotal,'.',''), ',', '.')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(translate(AreaTotal,'.',''), ',', '.'), 'm²', ''),'M²','')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="QtdDormitorios"/></bedroom>
            <bathroom><xsl:value-of select="QtdBanheiros"/></bathroom>
            <suite><xsl:value-of select="QtdSuites"/></suite>
            <garage><xsl:value-of select="QtdVagas"/></garage>

            <description>&lt;![CDATA[
                <xsl:choose>
                    <xsl:when test="Observacao">
                        <xsl:value-of select="Observacao"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="observacao | MemorialDescritivo"/>
                    </xsl:otherwise>
                </xsl:choose>
                ]]&gt;
            </description>

            <xsl:choose>
                <xsl:when test="Pais">
                    <country><xsl:value-of select="Pais"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>


            <state><xsl:value-of select="UF | Estado"/></state>
            <city><xsl:value-of select="Cidade"/></city>
            <neighborhood><xsl:value-of select="Bairro"/></neighborhood>

            <latitude><xsl:value-of select="translate(latitude, 'X', '')"/></latitude>
            <longitude><xsl:value-of select="translate(longitude, 'Y', '')"/></longitude>

            <zipcode><xsl:value-of select="CEP"/></zipcode>
            <address><xsl:value-of select="Endereco"/></address>
            <street_number><xsl:value-of select="translate(Numero, 's/n', '')"/></street_number>
            <complement><xsl:value-of select="Complemento | ComplementoEndereco"/></complement>

            <photos>
                <xsl:for-each select="Fotos/Foto">
                    <photo>
                        <url><xsl:value-of select="URLArquivo"/></url>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
