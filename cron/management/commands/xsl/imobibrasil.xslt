<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Imobibrasil" match="/">
        <property>
            <referer><xsl:value-of select="id"/></referer>
            <title>&lt;![CDATA[<xsl:value-of select="titulo"/>]]&gt;</title>
            <category><xsl:value-of select="tipoimovel"/></category>
            <subcategory><xsl:value-of select="subtipoimovel"/></subcategory>

            <xsl:choose>
                <xsl:when test="transacao = 'Venda'">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:when test="transacao = 'Aluguel'">
                    <transaction>L</transaction>
                </xsl:when>
            </xsl:choose>

            <goal>R</goal>


            <xsl:choose>
                <xsl:when test="transacao = 'Venda'">
                    <sale_price>
                        <xsl:choose>
                            <xsl:when test="contains(valor, ',00')">
                                <xsl:value-of select="translate(substring-before(valor, ',00'), '.', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(valor, ',', '.')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </sale_price>
                </xsl:when>
                <xsl:when test="transacao = 'Aluguel'">
                    <rent_price>
                        <xsl:choose>
                            <xsl:when test="contains(valor, ',00')">
                                <xsl:value-of select="translate(substring-before(valor, ',00'), '.', '')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="translate(valor, ',', '.')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </rent_price>
                </xsl:when>
            </xsl:choose>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(area_construida, ',00')">
                        <xsl:value-of select="translate(substring-before(area_construida, ',00'), '.', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(area_construida, ',', '.')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(area_total, ',00')">
                        <xsl:value-of select="translate(substring-before(area_total, ',00'), '.', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(area_total, ',', '.')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(valor_condominio, ',00')">
                        <xsl:value-of select="translate(substring-before(valor_condominio, ',00'), '.', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(valor_condominio, ',', '.')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condo_price>
            <bedroom><xsl:value-of select="dormitorios"/></bedroom>
            <bathroom><xsl:value-of select="banheiro"/></bathroom>
            <suite><xsl:value-of select="suites"/></suite>
            <garage><xsl:value-of select="vagas"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="descricao"/>]]&gt;</description>
            <xsl:choose>
                <xsl:when test="endereco_pais">
                    <country><xsl:value-of select="endereco_pais"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>

            <zipcode><xsl:value-of select="endereco_cep"/></zipcode>
            <state><xsl:value-of select="endereco_estado"/></state>
            <city><xsl:value-of select="endereco_cidade"/></city>
            <zone><xsl:value-of select="endereco_zona"/></zone>
            <neighborhood><xsl:value-of select="endereco_bairro"/></neighborhood>

            <address><xsl:value-of select="endereco_logradouro"/></address>
            <street_number><xsl:value-of select="endereco_numero"/></street_number>
            <latitude><xsl:value-of select="Location/Latitude"/></latitude>
            <longitude><xsl:value-of select="Location/Longitude"/></longitude>
            <photos>
                <xsl:for-each select="fotos/foto">
                    <photo>
                        <url><xsl:value-of select="foto_url"/></url>
                        <position><xsl:value-of select="foto_ordem"/></position>
                    </photo>
                </xsl:for-each>
            </photos>
            <videos>
                <video><xsl:value-of select="video"/></video>
            </videos>
        </property>
    </xsl:template>
</xsl:stylesheet>
