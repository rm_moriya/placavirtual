<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Vista" match="/">
        <property>
            <referer><xsl:value-of select="CodigoImovel"/></referer>
            <category><xsl:value-of select="TipoImovel"/></category>
            <transaction>
                <xsl:choose>
                    <xsl:when test="Venda = 'Sim' and Locacao='Nao' and PrecoLocacaoTemporada=0">V</xsl:when>
                    <xsl:when test="Venda = 'Nao' and Locacao='Sim' and PrecoLocacaoTemporada=0">L</xsl:when>
                    <xsl:when test="Venda = 'Sim' and Locacao='Sim' and PrecoLocacaoTemporada=0">VL</xsl:when>
                    <xsl:when test="Venda = 'Sim' and Locacao='Sim' and PrecoLocacaoTemporada > 0">VLT</xsl:when>
                </xsl:choose>
            </transaction>

            <sale_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoVenda, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoVenda, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(PrecoVenda, ',', '.'), 'R$', ''), '.', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </sale_price>

            <rent_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoLocacao, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoLocacao, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(PrecoLocacao, ',', '.'), 'R$', ''), '.', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </rent_price>

            <season_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoLocacaoTemporada, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoLocacaoTemporada, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(translate(PrecoLocacaoTemporada, ',', '.'), 'R$', ''), '.', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </season_price>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(AreaUtil, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(AreaUtil, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(AreaUtil, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <bedroom><xsl:value-of select="QtdDormitorios"/></bedroom>
            <bathroom><xsl:value-of select="QtdBanheiros"/></bathroom>
            <suite><xsl:value-of select="QtdSuites"/></suite>
            <garage><xsl:value-of select="QtdVagas"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="Descricao"/>]]&gt;</description>

            <country>
                <xsl:choose>
                    <xsl:when test="Pais">
                        <xsl:value-of select="Pais"/>
                    </xsl:when>
                    <xsl:otherwise>BR</xsl:otherwise>
                </xsl:choose>
            </country>
            <state><xsl:value-of select="UF"/></state>
            <city><xsl:value-of select="Cidade"/></city>
            <neighborhood><xsl:value-of select="Bairro"/></neighborhood>

            <latitude><xsl:value-of select="GMapsLatitude"/></latitude>
            <longitude><xsl:value-of select="GMapsLongitude"/></longitude>

            <zipcode><xsl:value-of select="CEP"/></zipcode>
{#            <address><xsl:value-of select="endereco"/></address>#}
{#            <complement><xsl:value-of select="complemento"/></complement>#}

            <photos>
                <xsl:for-each select="Fotos/Foto">
                    <photo>
                        <url><xsl:value-of select="URL"/></url>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
