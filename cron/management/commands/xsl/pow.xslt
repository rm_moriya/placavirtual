<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Pow" match="/">
        <property>
            <referer><xsl:value-of select="ref"/></referer>
            <title><xsl:value-of select="nome"/></title>
            <category><xsl:value-of select="tipoimovel"/></category>
            <xsl:choose>
                <xsl:when test="venda = 1 and locacao = 1">
                    <transaction>VL</transaction>
                </xsl:when>
                <xsl:when test="venda = 1">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:when test="locacao = 1">
                    <transaction>L</transaction>
                </xsl:when>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="comercial = 1">
                    <goal>C</goal>
                </xsl:when>
                <xsl:when test="residencial = 1">
                    <goal>R</goal>
                </xsl:when>
            </xsl:choose>

            <sale_price>
                <xsl:choose>
                    <xsl:when test="contains(preco_venda, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(preco_venda, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(preco_venda, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </sale_price>

            <rent_price>
                <xsl:choose>
                    <xsl:when test="contains(preco_locacao, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(preco_locacao, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(preco_locacao, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </rent_price>

            <condo_price>
                <xsl:choose>
                    <xsl:when test="contains(condominio_valor, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(condominio_valor, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(condominio_valor, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condo_price>

            <season_price>
                <xsl:choose>
                    <xsl:when test="contains(preco_temporada, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(preco_temporada, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(preco_temporada, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </season_price>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(area_util, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(area_util, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(area_util, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(area_terreno, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(area_terreno, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(area_terreno, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="quartos"/></bedroom>
            <bathroom><xsl:value-of select="banheiro"/></bathroom>
            <garage><xsl:value-of select="vagas"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="descricao"/>]]&gt;</description>

            <xsl:choose>
                <xsl:when test="Pais">
                    <country><xsl:value-of select="Pais"/></country>
                </xsl:when>
                <xsl:otherwise>
                    <country>BR</country>
                </xsl:otherwise>
            </xsl:choose>
            <state><xsl:value-of select="estado"/></state>
            <city><xsl:value-of select="cidade"/></city>
            <zone><xsl:value-of select="zona"/></zone>
            <neighborhood><xsl:value-of select="bairro"/></neighborhood>

            <latitude><xsl:value-of select="latitude"/></latitude>
            <longitude><xsl:value-of select="longitude"/></longitude>

            <zipcode><xsl:value-of select="cep"/></zipcode>
            <address><xsl:value-of select="endereco"/></address>
            <street_number><xsl:value-of select="translate(numero, 'SN', '')"/></street_number>
            <complement><xsl:value-of select="complemento"/></complement>

            <photos>
                <xsl:for-each select="fotos/foto">
                    <photo>
                        <url><xsl:value-of select="foto_url"/></url>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
