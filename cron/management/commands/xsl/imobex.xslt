<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Imobex" match="/">
        <property>
            <referer><xsl:value-of select="ref"/></referer>
            <category><xsl:value-of select="tipoimovel"/></category>
            <xsl:choose>
                <xsl:when test="transacao = 'Venda'">
                    <transaction>V</transaction>
                </xsl:when>
                <xsl:otherwise>
                    <transaction>L</transaction>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="contains(subtipoimovel, 'Comercial') or contains(subtipoimovel, 'Comercial')">
                    <goal>C</goal>
                </xsl:when>
                <xsl:otherwise>
                    <goal>R</goal>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:if test="transacao = 'Venda'">
                <sale_price>
                    <xsl:choose>
                        <xsl:when test="contains(valor, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(valor, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(valor, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </sale_price>
            </xsl:if>

            <xsl:if test="transacao = 'Locação'">
                <rent_price>
                    <xsl:choose>
                        <xsl:when test="contains(valor, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(valor, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(valor, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </rent_price>
            </xsl:if>

            <xsl:if test="transacao = 'Temporada'">
                <rent_high_season_price>
                    <xsl:choose>
                        <xsl:when test="contains(valor_alta_temporada, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(valor_alta_temporada, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(valor_alta_temporada, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </rent_high_season_price>
            </xsl:if>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(area_privativa, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(area_privativa, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(area_privativa, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <total_area>
                <xsl:choose>
                    <xsl:when test="contains(area_total, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(area_total, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(area_total, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </total_area>

            <bedroom><xsl:value-of select="dormitorios"/></bedroom>
            <bathroom><xsl:value-of select="banheiro"/></bathroom>
            <suite><xsl:value-of select="suites"/></suite>
            <garage><xsl:value-of select="vagas"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="descricao"/>]]&gt;</description>

            <country><xsl:value-of select="pais"/></country>
            <state><xsl:value-of select="estado"/></state>
            <city><xsl:value-of select="cidade"/></city>
            <zone><xsl:value-of select="zona"/></zone>
            <neighborhood><xsl:value-of select="bairro"/></neighborhood>
            <latitude><xsl:value-of select="latitude"/></latitude>
            <longitude><xsl:value-of select="longitude"/></longitude>

            <zipcode><xsl:value-of select="cep"/></zipcode>
            <address><xsl:value-of select="endereco"/></address>
            <complement><xsl:value-of select="complemento"/></complement>

            <photos>
                <xsl:for-each select="fotos/foto">
                    <photo>
                        <url><xsl:value-of select="foto_url"/></url>
                        <order><xsl:value-of select="foto_ordem"/></order>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
