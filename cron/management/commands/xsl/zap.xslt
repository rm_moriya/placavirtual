<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>

    <xsl:template name="Zap" match="/">
        <property>
            <referer><xsl:value-of select="CodigoImovel"/></referer>
            <category><xsl:value-of select="TipoImovel"/></category>

            <!-- ?? -->
            <transaction><xsl:value-of select="TipoOferta"/></transaction>


            <sale_price>
                <xsl:choose>
                    <xsl:when test="contains(PrecoVenda, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(PrecoVenda, ',00'), '.', ''), 'R$', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(PrecoVenda, ',', '.'), 'R$', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </sale_price>

            <xsl:if test="transacao = 'Locação'">
                <rent_price>
                    <xsl:choose>
                        <xsl:when test="contains(valor, ',00')">
                            <xsl:value-of select="translate(translate(substring-before(valor, ',00'), '.', ''), 'R$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate(translate(valor, ',', '.'), 'R$', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </rent_price>
            </xsl:if>

            <private_area>
                <xsl:choose>
                    <xsl:when test="contains(AreaUtil, ',00')">
                        <xsl:value-of select="translate(translate(substring-before(AreaUtil, ',00'), '.', ''), 'm²', '')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate(AreaUtil, ',', '.'), 'm²', '')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </private_area>

            <bedroom><xsl:value-of select="QtdDormitorios"/></bedroom>
            <bathroom><xsl:value-of select="QtdBanheiros"/></bathroom>
            <suite><xsl:value-of select="QtdSuites"/></suite>
            <garage><xsl:value-of select="QtdVagas"/></garage>

            <description>&lt;![CDATA[<xsl:value-of select="Observacao"/>]]&gt;</description>

            <country><xsl:value-of select="pais"/></country>
            <state><xsl:value-of select="estado"/></state>
            <city><xsl:value-of select="Cidade"/></city>
            <zone><xsl:value-of select="zona"/></zone>
            <neighborhood><xsl:value-of select="Bairro"/></neighborhood>

            <latitude><xsl:value-of select="latitude"/></latitude>
            <longitude><xsl:value-of select="longitude"/></longitude>

            <zipcode><xsl:value-of select="cep"/></zipcode>
            <address><xsl:value-of select="endereco"/></address>
            <complement><xsl:value-of select="complemento"/></complement>

            <photos>
                <xsl:for-each select="fotos/foto">
                    <photo>
                        <url><xsl:value-of select="foto_url"/></url>
                    </photo>
                </xsl:for-each>
            </photos>
        </property>
    </xsl:template>
</xsl:stylesheet>
