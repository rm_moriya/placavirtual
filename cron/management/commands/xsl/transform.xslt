<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="fileName" select="''"/>
    <xsl:include href="viva_real.xslt" />
    <xsl:include href="webgestor_viva_real_gaiawebservice_zap.xslt" />
    <xsl:include href="minha_primeira_casa.xslt" />
    <xsl:include href="chaves_na_mao.xslt" />
    <xsl:include href="imobex.xslt" />
    <xsl:include href="trovit.xslt" />
    <xsl:include href="casa_soft.xslt" />
    <xsl:include href="imobibrasil.xslt" />
    <xsl:include href="vista.xslt" />
    <xsl:include href="imofox.xslt" />
    <xsl:include href="imobox.xslt" />
    <xsl:include href="pow.xslt" />
    <xsl:include href="imovel_pro.xslt" />
    <xsl:include href="simbo.xslt" />

    <xsl:template match="/">
        <real_estate>
            <xsl:choose>
                <!--
                Simbo
                http://xml.simbo.com.br/579/ee528610-a0d7-43ab-bbf4-f5c36ebfdbd2.xml
                -->
                <xsl:when test="SimboRealtyFeed/SimboRealtyList/SimboRealty">
                    <xsl:for-each select="SimboRealtyFeed/SimboRealtyList/SimboRealty"><xsl:call-template name="Simbo"/></xsl:for-each>
                </xsl:when>
                <!--
                Imovel pro
                http://integracao.imovelpro.com.br/integracao/exportacao_imoveis_url.cfm?portal=ygOSkwmCT96a4C9B&hash=vFAg1ukHoepoT3cG
                -->
                <xsl:when test="exportacao/imovel">
                    <xsl:for-each select="exportacao/imovel"><xsl:call-template name="ImovelPro"/></xsl:for-each>
                </xsl:when>
                <!--
                pow
                http://www.coelhoimoveis.com.br/powsites/81672/xml/integra.xml
                -->
                <xsl:when test="pow/imovel">
                    <xsl:for-each select="pow/imovel"><xsl:call-template name="Pow"/></xsl:for-each>
                </xsl:when>
                <!--
                imofox
                http://edisonborges.com.br/xml-imofox.php
                -->
                <xsl:when test="imofox/imoveis/imovel">
                    <xsl:for-each select="imofox/imoveis/imovel"><xsl:call-template name="Imofox"/></xsl:for-each>
                </xsl:when>
                <!--
                iMOBOX
                http://portais.integracao.sci20.com.br/imagine/imobox_carga.xml
                -->
                <xsl:when test="imoveis/imovel | Imoveis/imovel">
                    <xsl:for-each select="imoveis/imovel | Imoveis/imovel"><xsl:call-template name="Imobox"/></xsl:for-each>
                </xsl:when>
                <!--
                Vista
                http://praminhacasa.com.br/vista.xml
                -->
                <xsl:when test="Anuncios/Imovel">
                    <xsl:for-each select="Anuncios/Imovel"><xsl:call-template name="Vista"/></xsl:for-each>
                </xsl:when>
                <!--
                Imobibrasil
                http://www.viverpr.com.br/feed/imobibrasil/v10_www.viverpr.com.br.xml
                -->
                <xsl:when test="imobibrasil/imovel">
                    <xsl:for-each select="imobibrasil/imovel"><xsl:call-template name="Imobibrasil"/></xsl:for-each>
                </xsl:when>
                <!--
                Viva Real
                http://www.integracaounion.com.br/vivareal/11991_vrsync.xml
                -->
                <xsl:when test="ListingDataFeed/Listings/Listing">
                    <xsl:for-each select="ListingDataFeed/Listings/Listing"><xsl:call-template name="VivaRealImovel"/></xsl:for-each>
                </xsl:when>
                <!--
                Viva Real
                http://www.integracaounion.com.br/vivareal/11991_vrsync.xml
                -->
                <xsl:when test="ListingDataFeed/Listing">
                    <xsl:for-each select="ListingDataFeed/Listing"><xsl:call-template name="VivaRealImovel"/></xsl:for-each>
                </xsl:when>
                <!--
                Chaves na mão / MPC
                http://imoveisxml.apolar.net/PadraoApolarChaves.xml
                -->
                <xsl:when test="Document/imoveis/imovel">
                    <xsl:for-each select="Document/imoveis/imovel"><xsl:call-template name="ChavesNaMao"/></xsl:for-each>
                </xsl:when>
                <!--
                Imobex
                http://repo.lenova.com.br/mpc/100003/imobex.xml
                -->
                <xsl:when test="imobex/imovel">
                    <xsl:for-each select="imobex/imovel"><xsl:call-template name="Imobex"/></xsl:for-each>
                </xsl:when>
                <!--
                Casa Soft
                http://praminhacasa.com.br/casasoft710L11.xml
                http://praminhacasa.com.br/casasoft710V11.xml
                -->
                <xsl:when test="indicadordeimoveis/fichaimoveis/imovel">
                    <xsl:for-each select="indicadordeimoveis/fichaimoveis/imovel"><xsl:call-template name="Casasoft"/></xsl:for-each>
                </xsl:when>
                <!--
                Trovit
                -->
                <xsl:when test="trovit/ad">
                    <xsl:for-each select="trovit/ad"><xsl:call-template name="Trovit"/></xsl:for-each>
                </xsl:when>
                <!--
                Web gestor - Viva Real
                http://web-gestor.net/wg5/arq/01202_94895/exps/vr.xml
                Zap
                http://www.redeimoveis.com.br/exportadores/geral.php?imob_codigo=8&tipo=zap
                Gaia Web Service
                http://www.valuegaia.com.br/integra/midia.ashx?midia=GaiaWebServiceImovel&p=B43PicGKnTmG4zA%2fKz8UWFX9F5ySywg0mTwx8VUKmIEOcUyFkXiJyt7%2bA%2bXy%2bB0Wwm7Q99slqvLmfDSklOA4yg%3d%3d
                -->
                <xsl:when test="Carga/Imoveis/Imovel">
                    <xsl:for-each select="Carga/Imoveis/Imovel"><xsl:call-template name="WebGestorVivaReal_GaiaWebService_Zap"/></xsl:for-each>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </real_estate>
    </xsl:template>
</xsl:stylesheet>
