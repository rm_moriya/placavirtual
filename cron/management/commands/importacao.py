#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import HTMLParser
import os
import lxml.etree as ET
import codecs
import datetime
from django.core.management.base import BaseCommand
from django.utils.text import slugify
from django.conf import settings
from advertiser.models import Xml, Account
from os.path import abspath, dirname
from lxml import objectify
from real_estate.models import Propriedade, Categoria
from log.models import Xml as LogXml


def translate_charset(path_name):
    h = HTMLParser.HTMLParser()
    new_path_name = "%s.charset" % path_name

    chardetect = os.popen("file -b %s" % path_name).read().upper()

    if 'UTF-16' in chardetect:
        charset = 'UTF16'
    else:
        charset = os.popen("chared -m portuguese %s" % path_name).read().split(':')[1].strip().replace('_', '')\
            .replace('-', '').upper()

    f = open(path_name, 'r+')
    xml_content = h.unescape(f.read().decode(charset).encode('ascii', 'xmlcharrefreplace')).encode('UTF-8')
    f.close()

    xml_content = re.sub(r"encoding=\"([a-zA-Z0-9\-][^\?])+\"", 'encoding="UTF-8"', xml_content)
    f = open(new_path_name, 'w+')
    f.write(xml_content)
    f.close()

    if os.path.isfile(path_name):
        os.remove(path_name)

    if os.path.isfile(new_path_name):
        os.rename(new_path_name, path_name)


def xslt(xml_id, vista=None):
    try:
        h = HTMLParser.HTMLParser()

        xml_content = open("/var/tmp/%s.xml" % xml_id).read()

        if '<Observacao><![CDATA[' not in xml_content:
            xml_content = xml_content.replace('<Observacao>', '<Observacao><![CDATA[').replace('</Observacao>', ']]></Observacao>')

        xml_content = xml_content.replace('<li>', '')
        xml_content = xml_content.replace('encoding="ISO8859-1"', '')

        xml_content = xml_content.replace('xmlns="http://www.vivareal.com/schemas/1.0/VRSync"', '')
        xml_content = xml_content.replace("xmlns='http://www.vivareal.com/schemas/1.0/VRSync'", '')

        xml_content = xml_content.replace('xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"', '')
        xml_content = xml_content.replace("xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'", '')
        xml_content = xml_content.replace('xmlns:xsd="http://www.w3.org/2001/XMLSchema"', '')

        xml_content = xml_content.replace('xsi:schemaLocation="http://www.vivareal.com/schemas/1.0/VRSync"', '')

        xml_content = xml_content.replace("xsi:schemaLocation='http://www.vivareal.com/schemas/1.0/VRSync  http://xml.vivareal.com/vrsync.xsd'", '')
        xml_content = xml_content.replace('xsi:schemaLocation="http://www.vivareal.com/schemas/1.0/VRSync  http://xml.vivareal.com/vrsync.xsd"', '')
        xml_content = xml_content.replace('xsi:schemaLocation="http://www.vivareal.com/schemas/1.0/VRSync http://xml.vivareal.com/vrsync.xsd"', '')

        xml_content = xml_content.replace('xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema""', '')
        xml_content = xml_content.replace('http://xml.vivareal.com/vrsync.xsd"', '')
        xml_content = xml_content.replace('xmlns:xsd="http://www.w3.org/2001/XMLSchema"', '')
        xml_content = xml_content.replace('', '')
        xml_content = xml_content.replace('', '')
        xml_content = xml_content.replace('\n"', '')
        xml_content = xml_content.replace('<p>', '')
        xml_content = xml_content.replace('<br>', '').replace('<BR>', '').replace('<br >', '').replace('<br />', '')

        xml_content = re.sub(r"&(?!#?[a-z0-9]+);", '', xml_content).replace('&', 'e')

        dom = ET.fromstring(xml_content)
        xslt = ET.parse(abspath(dirname(__file__)) + '/xsl/transform.xslt')
        transform = ET.XSLT(xslt)
        arq = file("/var/tmp/%s.xml" % xml_id, "w")
        arq.write(h.unescape(ET.tostring(transform(dom), pretty_print=True)).encode('UTF-8'))
        arq.close()

    except Exception, e:
        print "erro no id:" + str(xml_id)
        print e


def download(xml):

    tmp_name = '/var/tmp/' + str(xml.id) + '.xml'
    print xml.url
    os.popen("wget --connect-timeout=5 --tries=3 '%s' -O '%s'" % (str(xml.url).strip(), tmp_name))

    xml_content = open("/var/tmp/%s.xml" % xml.id).read()
    if not '<?xml' in xml_content:
        header = "-U 'Mozilla/5.0 (X11; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0'"
        os.popen("wget %s --connect-timeout=5 --tries=3 '%s' -O '%s'" % (header, str(xml.url).strip(), tmp_name))

    if os.path.getsize(tmp_name) == 0:
        return False
    return tmp_name


def import_xml(xml):
    # Deleta todos os logs do xml
    LogXml.objects.filter(xml=xml.id).delete()

    path_name = '/var/tmp/' + str(xml.id) + '.xml'
    with codecs.open(path_name, "r", encoding='utf8') as file_xml:
        xml_file = file_xml.read()

    real_estate = objectify.fromstring(xml_file)

    xml.total_property = len(real_estate.property)

    property_photo = 0
    for property_xml in real_estate.property:
        try:
            if Propriedade.objects.filter(referer=unicode(property_xml.referer), account=xml.account).exists():
                property = Propriedade.objects.get(referer=unicode(property_xml.referer), account=xml.account)
            else:
                property = Propriedade()

            # Verifica se o anuncio é de uma loja cadastrada
            if property_xml.find('strore_referer') and property_xml.strore_referer:
                try:
                    profile = Account.objects.get(referer=property_xml.strore_referer)
                    property.account = profile
                except Exception:
                    property.account = xml.account

            else:
                property.account = xml.account

            # Verifica se tem cod referencia
            if property_xml.find('referer') and property_xml.referer:
                property.referer = unicode(property_xml.referer)
            else:
                raise Exception("REF: " + str(unicode(property_xml.referer)) + ". Está sem cód de referência")

            if not property_xml.find('photos') is not None or len(property_xml.photos) == 0:
                raise Exception("REF: " + str(property_xml.referer) + ". Sem foto")

            # Verifica a categoria
            has_category = False
            category = None
            if property_xml.find('subcategory') and property_xml.subcategory:
                try:
                    category = Categoria.objects.get(slug=slugify(unicode(property_xml.subcategory)))
                    if category.pai:
                        category = Categoria.objects.get(pk=category.pai.pk)
                    property.categoria = category
                    has_category = True
                except Categoria.DoesNotExist:
                    pass

            if property_xml.find('category') and property_xml.category and has_category is False:
                try:
                    category = Categoria.objects.get(slug=slugify(unicode(property_xml.category)))
                    if category.pai:
                        category = Categoria.objects.get(pk=category.pai.pk)

                    property.category = category
                except Categoria.DoesNotExist:
                    pass
            elif has_category is False:
                pass

            # # Verifica o país
            # try:
            #     property.country = Country.objects.get(Q(slug=slugify(unicode(property_xml.country))) |
            #                                            Q(code=slugify(unicode(property_xml.country))))
            # except Country.DoesNotExist:
            #     log = LogCountry(name=slugify(unicode(property_xml.country)))
            #     log.save()
            #     raise Exception("REF: " + str(unicode(property_xml.referer)) + ". O valor passado no país não existe")
            #
            # # Verifica a cidade
            # try:
            #     if property_xml.find('state') and property_xml.state not in [None, '']:
            #         property.city = City.objects.get(Q(state__slug=slugify(unicode(property_xml.state))) |
            #                                          Q(state__code=slugify(unicode(property_xml.state))),
            #                                          slug=slugify(unicode(property_xml.city)))
            #     else:
            #
            #         try:
            #             city = City.objects.get(slug=slugify(unicode(property_xml.city)))
            #             if city.similar:
            #                 property.city = City.objects.get(pk=city.similar.pk)
            #             else:
            #                 property.city = city
            #         except City.MultipleObjectsReturned:
            #             log = LogCity(name=slugify(unicode(property_xml.city)), state=slugify(unicode(property_xml.state)))
            #             log.save()
            #
            #             if xml.account.state not in [None, '']:
            #                 property.city = City.objects.get(slug=slugify(unicode(property_xml.city)), state=xml.account.state)
            #
            #             pass
            #
            #     property.state = property.city.state
            # except City.DoesNotExist:
            #     log = LogCity(name=slugify(unicode(property_xml.city)), state=slugify(unicode(property_xml.state)))
            #     log.save()
            #     raise Exception("REF: " + str(unicode(property_xml.referer)) + ". O valor passado na cidade não existe")
            #
            # # Verifica o estado
            # if not property.state:
            #     try:
            #         property.state = State.objects.get(Q(slug=slugify(unicode(property_xml.state))) |
            #                                            Q(code=slugify(unicode(property_xml.state))),
            #                                            country=property.country.pk)
            #     except State.DoesNotExist:
            #         log = LogState(name=slugify(unicode(property_xml.state)))
            #         log.save()
            #         raise Exception("REF: " + str(unicode(property_xml.referer)) + ". O valor passado no estado não existe")
            #
            # # Verifica o bairro (opcional)
            # try:
            #     neighborhood = Neighborhood.objects.get(slug=slugify(unicode(property_xml.neighborhood)),
            #                                             city=property.city.pk)
            #     if neighborhood.similar:
            #         property.neighborhood = Neighborhood.objects.get(pk=neighborhood.similar.pk)
            #     else:
            #         property.neighborhood = neighborhood
            # except Neighborhood.DoesNotExist as e:
            #     log = LogNeighborhood(name=slugify(unicode(property_xml.neighborhood)), city=property.city.pk)
            #     log.save()
            #     pass

            # Verifica se tem transação
            if property_xml.find('transaction') and property_xml.transaction:
                property.transacao = property_xml.transaction
            else:
                raise Exception("REF: " + str(unicode(property_xml.referer)) + ". Está sem transação")

            # Verifica se tem transação
            if property_xml.find('goal') and property_xml.goal:
                property.finalidade = property_xml.goal
            else:
                raise Exception("REF: " + str(unicode(property_xml.referer)) + ". Está sem finalidade")

            # Verifica se tem titulo
            if property_xml.find('title') and property_xml.title:
                property.titulo = unicode(property_xml.title)
            else:
                if category is not None:
                    property.titulo = category.name
                    if slugify(property_xml.transaction) == 'v':
                        property.titulo += u' à venda'
                    elif slugify(property_xml.transaction) == 'a':
                        property.titulo += ' para alugar'

            property.valor_aluguel = float(property_xml.rent_price) if property_xml.find('rent_price') else 0
            property.valor_venda = float(property_xml.sale_price) if property_xml.find('sale_price') else 0

            property.endereco = unicode(property_xml.address) if property_xml.find('address') and property_xml.address else None
            property.email = unicode(property_xml.email) if property_xml.find('email') and property_xml.email else None
            property.complemento = unicode(property_xml.complement) if property_xml.find('complement') and property_xml.complement else None
            property.numero = unicode(property_xml.street_number) if property_xml.find('street_number') and property_xml.street_number else None
            property.latitude = float(property_xml.latitude) if property_xml.find('latitude') and property_xml.latitude else None
            property.longitude = float(property_xml.longitude) if property_xml.find('longitude') and property_xml.longitude else None
            property.descricao = unicode(property_xml.description) if property_xml.description else None
            property.area_privada = float(property_xml.private_area) if property_xml.find('private_area') and property_xml.private_area else 0
            property.area_total = float(property_xml.total_area) if property_xml.find('total_area') and property_xml.total_area else 0

            property.quartos = int(property_xml.bedroom) if property_xml.find('bedroom') and property_xml.bedroom else 0
            property.banheiros = int(property_xml.bathroom) if property_xml.find('bathroom') and property_xml.bathroom else 0
            property.suite = int(property_xml.suite) if property_xml.find('suite') and property_xml.suite else 0
            property.vagas = int(property_xml.garage) if property_xml.find('garage') and property_xml.garage else 0
            property.deletado = None
            property.save()

            if property_xml.find('photos') is not None and property_xml.photos:
                if property_xml.photos.find('photo') is not None:
                    property_photo += 1
                    position = 0
                    for photo in property_xml.photos.photo:
                        if photo.find('position') and photo.position:
                            position = photo.position
                        else:
                            position += 1

                        rate, created = property.fotos_set.get_or_create(url=unicode(photo.url))
                        rate.posicao = position
                        rate.deletado = None
                        rate.save()

        except Exception, e:
            print e
            log = LogXml(xml=xml, error=e.message, referer=unicode(property_xml.referer))
            log.save()
            pass

    # Deleta o XML depois de ler
    os.remove(path_name)

    # Grava data de leitura
    xml.total_property_photo = property_photo
    xml.date_readed = datetime.datetime.now()
    xml.save()


class Command(BaseCommand):
    args = '--id profile_ids'
    help = 'Importacao XML'

    def add_arguments(self, parser):
        parser.add_argument('--id', nargs='+', type=int, default=False)

    def handle(self, *args, **options):
        # LogCharacteristic.objects.all().delete()
        # LogCategory.objects.all().delete()
        # LogCity.objects.all().delete()
        # LogNeighborhood.objects.all().delete()

        if options['id']:
            xmls = Xml.objects.filter(account__in=map(int, options['id']))
            Propriedade.objects.filter(account__in=map(int, options['id'])).update(deletado=datetime.datetime.now())
        else:
            # Deleta todos os imoveis de profiles que tenham xml
            anunciantes = Account.objects.filter(xml__isnull=False)
            for anunciante in anunciantes:
                Propriedade.objects.filter(account=anunciante).update(deletado=datetime.datetime.now())

            xmls = Xml.objects.all()

        for xml in xmls:
            try:
                if download(xml):
                    translate_charset('/var/tmp/' + str(xml.id) + '.xml')
                    xslt(xml.id)
                    import_xml(xml)
            except Exception, e:
                pass