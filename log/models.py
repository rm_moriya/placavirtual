#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models


class Xml(models.Model):
    referer = models.CharField(max_length=70, null=False, blank=False)
    error = models.TextField(null=False, blank=False)
    xml = models.ForeignKey('advertiser.Xml')
