/**
 * Created by rafael on 01/12/15.
 */
var map
var newLat
var newLng
var markersPosition = []

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -25.455258, lng: -49.267242},
        zoomControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        zoom: 4
    })

    //google.maps.event.addListener(map,'dragend',getMoveData)
}
$(document).ready(function() {
    initMap()
    watchId = navigator.geolocation.watchPosition(
        successGeolocation,
        errorGeolocation,
        {enableHighAccuracy: true, timeout: 10000, maximumAge: 0}
    )
})


function successGeolocation(position) {
    if(position.coords.longitude != newLat && position.coords.longitude != newLng){
        clearMarkersPosition()
        map.setZoom(5)
        map.setCenter({
            lng: position.coords.longitude,
            lat: position.coords.latitude
        })
        newLat = position.coords.latitude
        newLng = position.coords.longitude
        showPlaces()

         var point = new google.maps.LatLng(
            parseFloat(position.coords.latitude),
            parseFloat(position.coords.longitude)
        )

        var marker = new MarkerWithLabel({
            position: point,
            draggable: false,
            raiseOnDrag: true,
            map: map,
            labelContent: "Você está aqui",
            labelAnchor: new google.maps.Point(0, 0),
            labelClass: "map-marker",
            icon: '/static/img/icons/tranparent.png'
        })
        markersPosition.push(marker)
    }
}

function errorGeolocation(error) {
    switch (error.code)
    {
        case error.TIMEOUT:
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    successGeolocation,
                    errorGeolocation,
                    {enableHighAccuracy: false, timeout: 10000, maximumAge: 0}
                );
            }
            else {
                alert("Este navegador não suporta a geolocalização");
            }
            break;
        case error.POSITION_UNAVAILABLE:
            latitude = 0;
            longitude = 0;
            alert('Não foi possivel localizar sua posição, tente novamente em um lugar aberto.');
            break;
        case error.PERMISSION_DENIED:
            latitude = 0;
            longitude = 0;
            alert('Permissão negada, por favor libere o acesso nas configurações do aparelho.');
            break;
        case error.UNKNOWN_ERROR:
            alert('Unknown error');
            break;
    }
}


function clearMarkersPosition() {
    for (var i = 0; i < markersPosition.length; i++) {
        markersPosition[i].setMap(null)
    }
    markersPosition = []
}


function showPlaces(){
    $.ajax({
        type: "POST",
        url: "/real-estate/ajax/imoveis-maps/",
        data: 'latitude=' + newLat + '&longitude=' + newLng + '&account=' + account,
        success: function(data) {
            var i = 0
            $.each(data.properties, function(key,value) {
                var point = new google.maps.LatLng(
                    parseFloat(value.latitude),
                    parseFloat(value.longitude)
                )

                var marker = new MarkerWithLabel({
                    position: point,
                    draggable: false,
                    raiseOnDrag: true,
                    map: map,
                    labelContent: '',
                    labelAnchor: new google.maps.Point(0, 0),
                    labelClass: "map-marker",
                    icon: '/static/site/img/icons/placa-virtual-mobile.png',
                    url: value.url
                })

                marker.addListener('click', function() {

                });
                markersPosition.push(marker)
                i++
            })
        }
    })
}