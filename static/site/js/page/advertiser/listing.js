/**
 * Created by rafael on 01/12/15.
 */
var map
var newLat
var newLng
var watchId

$(document).ready(function() {
    watchId = navigator.geolocation.watchPosition(
        successGeolocation,
        errorGeolocation,
        {enableHighAccuracy: true, timeout: 10000, maximumAge: 0}
    )
})


function successGeolocation(position) {
    if(position.coords.longitude != newLat && position.coords.longitude != newLng){
        newLat = position.coords.latitude
        newLng = position.coords.longitude
        showPlaces()
    }
}

function errorGeolocation(error) {
    switch (error.code)
    {
        case error.TIMEOUT:
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    successGeolocation,
                    errorGeolocation,
                    {enableHighAccuracy: false, timeout: 10000, maximumAge: 0}
                );
            }
            else {
                alert("Este navegador não suporta a geolocalização");
            }
            break;
        case error.POSITION_UNAVAILABLE:
            latitude = 0;
            longitude = 0;
            alert('Não foi possivel localizar sua posição, tente novamente em um lugar aberto.');
            break;
        case error.PERMISSION_DENIED:
            latitude = 0;
            longitude = 0;
            alert('Permissão negada, por favor libere o acesso nas configurações do aparelho.');
            break;
        case error.UNKNOWN_ERROR:
            alert('Unknown error');
            break;
    }
}

function showPlaces(){
    $.ajax({
        type: "POST",
        url: "/real-estate/ajax/imoveis-maps/",
        data: 'latitude=' + newLat + '&longitude=' + newLng + '&account=' + account,
        success: function(data) {
            $.each(data.properties, function(key,value) {
                $("#real_estates").append(value.listing)
            })
            navigator.geolocation.clearWatch(watchId)
        }
    })
}