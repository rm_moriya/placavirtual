$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true,
        lazyLoad: true,
        pagination: false,
        navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        rewindNav:false,
        navigation:false
    });
});