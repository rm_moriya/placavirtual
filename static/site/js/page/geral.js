/**
 * Created by rafael on 01/12/15.
 */
$(document).ready(function(){
    $('.ajax').on('submit', function(){
        var form = $(this)
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function(data) {
                if (data.status == true) {
                    form[0].reset()
                    alert('Sua mensagem foi enviada com sucesso!')
                }else{
                    alert('Preencha os campos corretamente!')
                }
            }
        })
        return false;
    })
})