/**
 * Created by rafael on 04/12/15.
 */
var graficoA
var graficoT
var graficoC
$(document).ready(function(){
    Morris.Line({
        element: 'grafico-acesso',
        data: graficoA,
        xkey: 'y',
        ykeys: ['a'],
        labels: ['acessos'],
        lineColors:['#0aa699','#d1dade']
	});
    Morris.Line({
        element: 'grafico-telefone',
        data: graficoT,
        xkey: 'y',
        ykeys: ['a'],
        labels: ['telefone'],
        lineColors:['#0aa699','#d1dade']
	});
    Morris.Line({
        element: 'grafico-direto',
        data: graficoC,
        xkey: 'y',
        ykeys: ['a'],
        labels: ['direto'],
        lineColors:['#0aa699','#d1dade']
	});
})