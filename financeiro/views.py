__author__ = 'rafael'
# -*- coding: utf-8 -*-
from datetime import datetime
from django.shortcuts import render
from base.models import Assinatura, Fatura


def recebe_fatura(request):
    if request.method == 'POST':
        assinatura = request.POST.get('subscription_id')
        id = request.POST.get('id')
        status = request.POST.get('status')
        try:
            fatura = Fatura.objects.get(iugu_id=id)
            if status == 'paid':
                fatura.data_pagamento = datetime.now()
                fatura.save()
        except Fatura.DoesNotExist:
            Fatura(assinatura=Assinatura.objects.get(iugu_id=assinatura), iugu_id=id).save()

    return render(request, 'teste.html', {})